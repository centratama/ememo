<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/assets/logo2.jpg">
    <title>Dashboard - eMemo | Centratama Group</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    <!-- chartist CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/chartist-js/dist/chartist.min.css') !!}
    {!! Html::style('assets/material-lite/assets/plugins/chartist-js/dist/chartist-init.css') !!}
    {!! Html::style('assets/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') !!}
    <!--This page css - Morris CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/c3-master/c3.min.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('assets/material-lite/lite/css/style.css') !!}
    <!-- You can change the theme colors from here -->
    {!! Html::style('assets/material-lite/lite/css/colors/blue.css') !!}

    <!-- Sweetalert CSS & JS-->
    {!! Html::style('assets/swal/dist/sweetalert.css') !!}
    {!! Html::script('assets/swal/dist/sweetalert.min.js') !!}

    <!-- Select2-->
    {!! Html::style('assets/material-lite/assets/plugins/select2/dist/css/select2.min.css') !!}

    <!-- Main JS-->
    {!! Html::script('assets/material-lite/assets/plugins/jquery/jquery.min.js') !!}

    <!-- Summernote-->
    {!! Html::style('assets/summernote/dist/summernote-lite.css') !!}
    {!! Html::script('assets/summernote/dist/summernote-lite.js') !!}

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    {!! Html::style('assets/emoji-picker/lib/css/emoji.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="background-color: white;">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            
                            <!-- Light Logo icon -->
                            <img src="{{url('/')}}/assets/logo2.jpg" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         <img src="{{url('/')}}/assets/logo1.jpg" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {{-- <img src="{{url('/')}}/assets/logo1.jpg" alt="user" class="profile-pic m-r-10" /> --}}
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <a href="#" class="link" data-toggle="modal" data-target="#changePassword">
                                            <i class="mdi mdi-key-variant text-red"></i> Change Password
                                        </a>
                                    </li>                            
                                    <li class="divider" role="separator"></li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="link">
                                            <i class="mdi mdi-power"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav list-unstyled">
                        <li class="nav-small-cap">
                            <center><i>{{date("d M, Y")}} <span id="clock"></span></i></center>
                        </li>
                        <li class="nav-small-cap">MAIN</li>
                        {{-- <li>
                            <a class="has-arrow waves-effect waves-dark">
                                <i class="mdi mdi-gauge"></i>
                                <span class="hide-menu">Amar</span>
                            </a>
                            <ul>
                                <li class="active"><a href="#" class="active">Parameters</a></li>
                                <li><a href="#">Parameters</a></li>
                                <li><a href="#">Parameters</a></li>
                                <li><a href="#">Parameters</a></li>
                            </ul>
                        </li> --}}
                        {{-- Menu --}}
                        {!! (new \App\Http\helpers)->getMenus(0) !!}
                        {{-- <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="link">
                                <i class="mdi mdi-power text-red"></i> Logout
                            </a>
                        </li> --}}
                    </ul>
                    <div class="text-center m-t-30">
                        <!-- <a href="https://wrappixel.com/templates/materialpro/" class="btn waves-effect waves-light btn-warning hidden-md-down"> Upgrade to Pro</a> -->
                    </div>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">
                            @if(!empty($data['title']))
                                {{$data['title']}}
                            @else
                                Dashboard
                            @endif
                        </h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            @if(!empty($data['title']))
                                <li class="breadcrumb-item active">{{$data['title']}}</li>
                            @endif
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <!-- <a href="https://wrappixel.com/templates/materialpro/" class="btn waves-effect waves-light btn-danger pull-right hidden-sm-down"> Upgrade to Pro</a> -->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                @include('layouts.notif')
                @yield('content')

                {{-- Modal Change Password --}}
                    <div id="changePassword" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="createGroupLabel" class="modal-title">Change Password</h4>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                </div>
                                <form class="form-horizontal form-material" method="POST" action="{{ url('password/change') }}">
                                    <div class="modal-body">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="password">New Password</label>
                                                <input type="password" class="form-control form-control-line" name="password" id="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="password_confirmation">Confirm New Password</label>
                                                <input type="password" class="form-control form-control-line" name="password_confirmation" id="password_confirmation">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                {{-- End Modal Change Password --}}
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> eMemo 0.1 © {{date('Y')}} <a href="https://www.centratamagroup.com">Centratama Group</a> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/tether.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js') !!}
    <!-- slimscrollbar scrollbar JavaScript -->
    {!! Html::script('assets/material-lite/lite/js/jquery.slimscroll.js') !!}
    <!--Wave Effects -->
    {!! Html::script('assets/material-lite/lite/js/waves.js') !!}
    <!--Menu sidebar -->
    {!! Html::script('assets/material-lite/lite/js/sidebarmenu.js') !!}
    <!--stickey kit -->
    {!! Html::script('assets/material-lite/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') !!}
    <!--Custom JavaScript -->
    {!! Html::script('assets/material-lite/lite/js/custom.min.js') !!}
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    {!! Html::script('assets/material-lite/assets/plugins/chartist-js/dist/chartist.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') !!}
    <!--c3 JavaScript -->
    {!! Html::script('assets/material-lite/assets/plugins/d3/d3.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/c3-master/c3.min.js') !!}
    <!-- Chart JS -->
    {!! Html::script('assets/material-lite/lite/js/dashboard1.js') !!}
    <!-- Select2 JS -->
    {!! Html::script('assets/material-lite/assets/plugins/select2/dist/js/select2.min.js') !!}
    {!! Html::script('assets/tinymce/js/tinymce/tinymce.min.js') !!}

    {{-- Emoji Picker --}}
    {!! Html::script('assets/emoji-picker/lib/js/config.js') !!}
    {!! Html::script('assets/emoji-picker/lib/js/util.js') !!}
    {!! Html::script('assets/emoji-picker/lib/js/jquery.emojiarea.js') !!}
    {!! Html::script('assets/emoji-picker/lib/js/emoji-picker.js') !!}
    
    @include('sweet::alert')

    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
            $(".select2").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);

                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            }); 

            $(".summernote").summernote({
                tabsize: 3,
                height: 300
            });
            tinymce.init({
                selector: '.mytextarea',
                theme: 'modern',
                height: 500,
                plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media link contextmenu colorpicker textpattern help',
                toolbar: 'code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                image_advtab: true,
            });

            $(function() {
                // Initializes and creates emoji set from sprite sheet
                window.emojiPicker = new EmojiPicker({
                    emojiable_selector: '[data-emojiable=true]',
                    assetsPath: '../lib/img/',
                    popupButtonClasses: 'fa fa-smile-o'
                });
                // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
                // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
                // It can be called as many times as necessary; previously converted input fields will not be converted again
                window.emojiPicker.discover();
            });

        });

        <?php
            $today = getdate();
        ?>
        var d = new Date("{{date('Y-m-d H:i:s')}}");
        setInterval(function() {
            d.setSeconds(d.getSeconds() + 1);
            $('#clock').text((('0' + d.getHours()).slice(-2) +':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2) ));
        }, 1000);
    </script>
</body>

</html>
