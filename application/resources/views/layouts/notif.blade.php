@if ($errors->isEmpty())
	{{-- <div class="alert alert-info">
		<strong>Success!</strong>
	</div> --}}
@else
	<div class="alert alert-danger alert-dismissible">
		<strong>Oops!</strong> something went wrong.
	</div>
	<ul>
		@foreach($errors->all(':message') as $key => $input_error)
		<li>
			<span class="help-block" style="color: red;">
				<strong>{{ $input_error }}</strong>
			</span>
		</li>
		@endforeach
	</ul>
@endif