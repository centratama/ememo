<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		{!! Html::style('assets/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
		<!-- Main JS-->
    	{!! Html::script('assets/material-lite/assets/plugins/jquery/jquery.min.js') !!}

		{!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js') !!}
		<!-- Custom CSS -->
		<style type="text/css">
			body {
				font-family: 'Arial', sans-serif;
				font-size: 10px;
			}
			.page-break {
    			page-break-after: always;
			}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			@yield('content')
		</div>
	</body>
</html>