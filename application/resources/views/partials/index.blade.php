@if ((count($items->children) > 0) AND ($items->parent_menu == NULL))

    <li><a class="has-arrow waves-effect waves-dark" href="{!! $items->link !!}"><span class="hide-menu">{!! $items->menu_name !!}</span></a>

@else

    <li><a class="waves-effect waves-dark" href="{!! $items->link !!}">{!! $items->menu_name !!}</a>

@endif

    @if (count($items->children) > 0)

        <ul>

        @foreach($items->children as $items)

            @include('partials.index', $items)

        @endforeach

        </ul>

    @endif

    </li>