@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-md-12">
			<div class="row">
				
				<div class="col-md-2 card">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2>{{$data['sum']['total_memo']}}</h2>
								</div>
								<div class="">
									<h3><i class="mdi mdi-file-document mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								Total Memo
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-2 card card-info text-white">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2 class="text-white">{{$data['sum']['draft']}}</h2>
								</div>
								<div class="">
									<h3 class="text-white"><i class="mdi mdi-content-paste mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								Draft
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-2 card card-primary text-white">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2 class="text-white">{{$data['sum']['request']}}</h2>
								</div>
								<div class="">
									<h3 class="text-white"><i class="mdi mdi-account-edit mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								In Progress
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-2 card card-success text-white">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2 class="text-white">{{$data['sum']['approve']}}</h2>
								</div>
								<div class="">
									<h3 class="text-white"><i class="mdi mdi-clipboard-check mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								Approve
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-2 card card-warning text-white">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2 class="text-white">{{$data['sum']['revise']}}</h2>
								</div>
								<div class="">
									<h3 class="text-white"><i class="mdi mdi-autorenew mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								Revise
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-2 card card-danger text-white">
					<div class="card-block">
						<div class="card-body">
							<div class="row">
								<div class="col-md-8">
									<h2 class="text-white">{{$data['sum']['reject']}}</h2>
								</div>
								<div class="">
									<h3 class="text-white"><i class="mdi mdi-block-helper mdi-36px"></i></h3>
								</div>
							</div>
							<div class="card-title">
								Reject
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection