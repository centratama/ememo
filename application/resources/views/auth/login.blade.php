<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/assets/logo2.jpg">
    <title>Login &raquo; eMemo</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    <!-- chartist CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/chartist-js/dist/chartist.min.css') !!}
    {!! Html::style('assets/material-lite/assets/plugins/chartist-js/dist/chartist-init.css') !!}
    {!! Html::style('assets/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') !!}
    <!--This page css - Morris CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/c3-master/c3.min.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('assets/material-lite/lite/css/style.css') !!}
    <!-- You can change the theme colors from here -->
    {!! Html::style('assets/material-lite/lite/css/colors/blue.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <!-- <div id="main-wrapper"> -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <!-- <div class="page-wrapper"> -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <!-- Column -->
                    <div class="login-register" style="background-color: #4FC3F7;">
                        <div class="login-box card">
                            <div class="card-block">
                                <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
                                    {!! csrf_field() !!}
                                    <center>
                                        <h3>eMemo</h3>
                                    </center>
                                    <h3>Sign In</h3>
                                    <div class="form-group">
                                        <!-- <label for="example-email" class="col-md-12">Email</label> -->
                                        <div class="col-xs-12">
                                            <input type="text" placeholder="Username" class="form-control form-control-line" name="username" id="username" value="{{ old('username') }}">
                                            
                                            @if ($errors->has('username'))
                                                <span class="help-block" style="color: red;">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="example-email" class="col-md-12">Password</label> -->
                                        <div class="col-xs-12">
                                            <input type="password" placeholder="Password" class="form-control form-control-line" name="password" id="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block" style="color: red;">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {{-- <div class="col-md-12">
                                            <div class="checkbox pull-left p-t-0">
                                                <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label for="checkbox-signup"> Remember me </label>
                                            </div>
                                            <a href="#" class="text-dark pull-right">
                                                <i class="fa fa-lock"></i> Forgot pwd?
                                            </a>
                                        </div> --}}
                                    </div>
                                    <div class="form-group text-center">
                                        <!-- <label for="example-email" class="col-md-12">Password</label> -->
                                        <div class="col-xs-12">
                                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light">Log In</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="example-email" class="col-md-12">Password</label> -->
                                        {{-- <div class="col-sm-12">
                                            <p>Don't have an account? 
                                                <a href="{{'register'}}" class="text-info">
                                                    <b>Sign Up</b>
                                                </a>
                                            </p>
                                        </div> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        <!-- </div> -->
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    <!-- </div> -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    {!! Html::script('assets/material-lite/assets/plugins/jquery/jquery.min.js') !!}
    <!-- Bootstrap tether Core JavaScript -->
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/tether.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js') !!}
    <!-- slimscrollbar scrollbar JavaScript -->
    {!! Html::script('assets/material-lite/lite/js/jquery.slimscroll.js') !!}
    <!--Wave Effects -->
    {!! Html::script('assets/material-lite/lite/js/waves.js') !!}
    <!--Menu sidebar -->
    {!! Html::script('assets/material-lite/lite/js/sidebarmenu.js') !!}
    <!--stickey kit -->
    {!! Html::script('assets/material-lite/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') !!}
    <!--Custom JavaScript -->
    {!! Html::script('assets/material-lite/lite/js/custom.min.js') !!}
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    {!! Html::script('assets/material-lite/assets/plugins/chartist-js/dist/chartist.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') !!}
    <!--c3 JavaScript -->
    {!! Html::script('assets/material-lite/assets/plugins/d3/d3.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/c3-master/c3.min.js') !!}
    <!-- Chart JS -->
    {!! Html::script('assets/material-lite/lite/js/dashboard1.js') !!}
</body>

</html>
