@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					@if ((new \App\Http\permissions)->isAllowed('trans_memo_create'))
						<center>
							<a href="{{url('trans/memo-create')}}" class="btn btn-info btn-lg text-uppercase waves-effect waves-light"><i class="mdi mdi-plus"></i> Add New Memo Transaction</a>
						</center>
					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<div class="card-body table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Memo Code</th>
									<th>Character</th>
									<th>Created By</th>
									<th>Status</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@if($data['memo']->count() > 0)
									@foreach($data['memo'] as $key => $row)
										<tr>
											<td>{{++$key}}</td>
											<td>
												@foreach($data['allmemo'] as $memo)
													{!! $row->memo_id == $memo->id ? $memo->memo_code : "" !!}	
												@endforeach
											</td>
											<td>
												@foreach($data['memochar'] as $char)
													{!! $row->char_id == $char->id ? $char->char_name : "" !!}	
												@endforeach
												</td>
											<td>
												@foreach($data['employee'] as $employee)
												{!! $row->created_by == $employee->number ? $employee->fullname : "" !!}
												@endforeach
											</td>
											<td>
												<span class="{{$data['label'][$row->status]}}">
												@foreach($data['param'] as $param)
													{!! $row->status == $param->param_value ? $param->param_desc : "" !!}	
												@endforeach
												</span>
											</td>
											<td>
												<ul class="list-inline">
													
													@if ((new \App\Http\permissions)->isAllowed('trans_memo_view'))
													<li class="list-inline-item">
														<a href="{{url('trans/memo-view')}}/{{$row->id}}" alt="View Detail Memo Transaction" title="View Detail Memo Transaction"> <i class="mdi mdi-information-variant mdi-24px text-info"></i>
														</a>
													</li>
													@endif

													@if ((new \App\Http\permissions)->isAllowed('trans_memo_update'))
														@if(in_array($row->status, array(0,3)) && $row->created_by == (new \App\Http\permissions)->getNIK_byEmail())
															<li class="list-inline-item">
																<a href="{{url('trans/memo-edit')}}/{{$row->id}}" alt="Change Data Memo Transaction" title="Change Data Memo Transaction"> <i class="mdi mdi-pencil mdi-24px text-warning"></i>
																</a>
															</li>
														@endif
													@endif
												</ul>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="6">No data available in table</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection