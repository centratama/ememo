@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<form class="form-horizontal form-material" method="post" action="{{route('store-memo-trans')}}" enctype="multipart/form-data">
						{!! csrf_field() !!}
						<div class="card-body">
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo">Memo Code</label>
									<select name="memo" class="form-control form-control-line select2" id="memo_code">
										<option>-- Choose --</option>
										@foreach($data['memo'] as $memo)
											<option value="{{$memo->id}}">{{$memo->memo_code}} - {{$memo->memo_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_char">Memo Char</label>
									<select name="memo_char" class="form-control form-control-line select2" id="memo_char">
										<option>-- Choose --</option>
										@foreach($data['memochar'] as $char)
											<option value="{{$char->id}}">{{$char->char_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="background">Background</label>
									<textarea name="background" class="mytextarea"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="background">Attachment</label>
									<input type="file" name="background_attachment[]" class="form-control form-control-line" multiple>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="consideration">Consideration</label>
									<textarea name="consideration" class="mytextarea"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="consideration">Attachment</label>
									<input type="file" name="consideration_attachment[]" class="form-control form-control-line" multiple>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="points_to_be_app">Points to be Approved</label>
									<textarea name="points_to_be_app" class="mytextarea"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="points_to_be_app">Attachment</label>
									<input type="file" name="points_to_be_app_attachment[]" class="form-control form-control-line" multiple>
								</div>
							</div>
							<br>
							<div class="col-md-12 text-right">
								<button type="reset" class="btn btn-lg btn-danger">Reset</button>
								<button type="submit" class="btn btn-lg btn-success">Save as Draft</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection