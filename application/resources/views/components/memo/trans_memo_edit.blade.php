@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					{{ Form::model($data['transmemo'], array('route' => ['update-memo-trans', $data['transmemo']->id], 'method' => 'patch', 'class'=>'form-horizontal form-material', 'enctype' => 'multipart/form-data')) }}
						{!! csrf_field() !!}
						<div class="card-body">
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo">Memo Code</label>
									<select name="memo" class="form-control form-control-line select2" id="memo_code" disabled>
										<option>-- Choose --</option>
										@foreach($data['allmemo'] as $memo)
											<option value="{{$memo->id}}" {!! $memo->id == $data['transmemo']->memo_id ? "selected" : "" !!}>{{$memo->memo_code}} - {{$memo->memo_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_char">Memo Char</label>
									<select name="memo_char" class="form-control form-control-line select2" id="memo_char">
										<option>-- Choose --</option>
										@foreach($data['memochar'] as $char)
											<option value="{{$char->id}}" {!! $char->id == $data['transmemo']->char_id ? "selected" : "" !!}>{{$char->char_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="background">Background</label>
									<textarea name="background" class="mytextarea">{!! $data['transmemo']->background !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="background">Attachment</label>
										<input type="file" name="background_attachment[]" class="form-control form-control-line" multiple>
										<em style="color: red;">Ignore, if there's no change.</em>
									</div>
									<div class="col-md-6">
										{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->background_attachment, $data['transmemo']->trans_memo_code)) !!}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="consideration">Consideration</label>
									<textarea name="consideration" class="mytextarea">{!! $data['transmemo']->consideration !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="consideration">Attachment</label>
										<input type="file" name="consideration_attachment[]" class="form-control form-control-line" multiple>
										<em style="color: red;">Ignore, if there's no change.</em>
									</div>
									<div class="col-md-6">
										{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->consideration_attachment, $data['transmemo']->trans_memo_code)) !!}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="points_to_be_app">Points to be Approved</label>
									<textarea name="points_to_be_app" class="mytextarea">{!! $data['transmemo']->points_to_be_app !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<label for="points_to_be_app">Attachment</label>
										<input type="file" name="points_to_be_app_attachment[]" class="form-control form-control-line" multiple>
										<em style="color: red;">Ignore, if there's no change.</em>
									</div>
									<div class="col-md-6">
										{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->points_to_be_app_attachment, $data['transmemo']->trans_memo_code)) !!}
									</div>
								</div>
							</div>
							<br>
							<div class="col-md-12 text-right">
								<input type="hidden" name="id" value="{{$data['transmemo']->id}}">
								<input type="hidden" name="trans_memo_code" value="{{$data['transmemo']->trans_memo_code}}">
								<button type="reset" class="btn btn-lg btn-danger">Reset</button>
								<button type="submit" class="btn btn-lg btn-success">Update</button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection