@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>
							{{ $data['title'] }}
						</h3>
					</div>
					<div class="card-body">
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link active" href="#detail" data-toggle="tab">Detail</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#approval" data-toggle="tab">Approval</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#comment" data-toggle="tab">Comments</a>
							</li>
						</ul>
						<div class="tab-content tabcontent-border">
							<div id="detail" class="tab-pane active">
								<div class="card-block">
									<div class="row">
										<div class="col-md-3">
											<b>Transaction Memo Code</b>:
										</div>
										<div class="col-md-9">
											{!! $data['transmemo']->trans_memo_code !!}
										</div>
										<div class="col-md-3">
											<b>Memo Code</b>:
										</div>
										<div class="col-md-9">
											@foreach($data['allmemo'] as $memo)
												{!! $data['transmemo']->memo_id == $memo->id ? $memo->memo_code : "" !!}
											@endforeach
										</div>
										<div class="col-md-3">
											<b>Memo Char</b>:
										</div>
										<div class="col-md-9">
											@foreach($data['memochar'] as $char)
												{!! $data['transmemo']->char_id == $char->id ? $char->char_name : "" !!}	
											@endforeach
										</div>
										<div class="col-md-3">
											<b>Background</b>:
										</div>
										<div class="col-md-9">
											{!! $data['transmemo']->background !!}
										</div>
										<div class="col-md-3">
											<b>Attachment</b>:
										</div>
										<div class="col-md-9">
											{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->background_attachment, $data['transmemo']->trans_memo_code)) !!}
										</div>
										<div class="col-md-3">
											<b>Consideration</b>:
										</div>
										<div class="col-md-9">
											{!! $data['transmemo']->consideration !!}
										</div>
										<div class="col-md-3">
											<b>Attachment</b>:
										</div>
										<div class="col-md-9">
											{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->consideration_attachment, $data['transmemo']->trans_memo_code)) !!}
										</div>
										<div class="col-md-3">
											<b>Points To Be Approved</b>:
										</div>
										<div class="col-md-9">
											{!! $data['transmemo']->points_to_be_app !!}
										</div>
										<div class="col-md-3">
											<b>Attachment</b>:
										</div>
										<div class="col-md-9">
											{!! ((new \App\Http\helpers)->getAttachmentMemo($data['transmemo']->points_to_be_app_attachment, $data['transmemo']->trans_memo_code)) !!}
										</div>
										<div class="col-md-3">
											<b>Status</b>:
										</div>
										<div class="col-md-9">
											<span class="{{$data['label'][$data['transmemo']->status]}}">
											@foreach($data['param'] as $param)
												{!! $data['transmemo']->status == $param->param_value ? $param->param_desc : "" !!}	
											@endforeach
											</span>
										</div>
									</div>
								</div>
							</div>
							<div id="approval" class="tab-pane">
								@if($data['transmemo']->status > 0)
								<div class="card-block">
									<div class="row">
										<div class="col-md-1">Initiator:</div>
										<div class="col-md-3">
											@foreach($data['employee'] as $employee)
												{!! $employee->number == $data['transmemo']->created_by ? $employee->fullname : "" !!}
											@endforeach
										</div>
									</div>
									<div class="row">
										<div class="col-md-1">Date:</div>
										<div class="col-md-3">{!! date("d M, Y H:i:s", strtotime($data['transmemo']->initiated_date)) !!}</div>
									</div>
									<div class="row">
										<div class="col-md-1">Comment:</div>
										<div class="col-md-3">{!! $data['transmemo']->comment !!}</div>
									</div>
								</div>
								@endif
								<div class="card-body table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>Approval Name</th>
												<th>Approver</th>
												<th>Comment</th>
												<th>Date</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											@foreach($data['memoapproval'] as $key => $approval)
												<tr>
													<td>{{$loop->iteration}}.</td>
													<td>
														@foreach($data['approval'] as $approvaltype)
															{!! $approvaltype->id == $approval->approval_id ? $approvaltype->approval_name : "" !!}
														@endforeach
													</td>
													<td>
														@foreach($data['employee'] as $employee)
															{!! $employee->number == $approval->approver ? $employee->fullname : "" !!}
														@endforeach
													</td>
													<td>
														{!! $approval->comment !!}
													</td>
													<td>
														{!! $approval->dateInitiated != NULL ? date("d M, Y H:i:s", strtotime($approval->dateInitiated)) : "" !!}
													</td>
													<td>
														<span class="{{$data['label'][$approval->status]}}">
														@if($approval->status === 0)
															Waiting...
														@else
															@foreach($data['param'] as $param)
																{!! $approval->status == $param->param_value ? $param->param_desc : "" !!}
															@endforeach	
														@endif
														</span>
													</td>
													<td>
														@if($approval->status == 2)
															@if($key == 0)
																{!! ((new \App\Http\helpers)->getDateDiff($data['transmemo']->initiated_date, $approval->dateInitiated)) !!}
															@else
																{!! ((new \App\Http\helpers)->getDateDiff($data['memoapproval'][$key-1]->dateInitiated, $data['memoapproval'][$key]->dateInitiated)) !!}
															@endif
														@endif
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
							<div id="comment" class="tab-pane">
								<div class="card-body">
									<div class="card-block">
										<div class="timeline-centered">
											{!! (new \App\Http\helpers)->getComment($data['transmemo']->id, 0, $data['transmemo']->status) !!}
											@if((new \App\Http\permissions)->isAllowedReply($data['participate']))
												{!! (new \App\Http\helpers)->getReplyCommentModal($data['transmemo']->id) !!}
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card-footer">
					<div class="col-md-12 text-right">
				
					{{-- Start Jika status Approve --}}
						@if($data['transmemo']->status == 2)

							<a class="btn btn-primary" href="{{url('trans/memo-print/')}}/{{$data['transmemo']->id}}" target="_blank"><i class="mdi mdi-printer"></i> Print</a>
							<script type="text/javascript">
								function printMemo(id)
								{
									window.location.href="{{url('trans/memo-print/')}}/"+id;
								}
							</script>
						@endif
					{{-- End --}}

					{{-- Start - Jika pengguna adalah yang membuat memo transaction dan status masih draft atau reject --}}
					@if ($data['transmemo']->created_by == (new \App\Http\permissions)->getNIK_byEmail(Auth::user()->email) && in_array($data['transmemo']->status, array(0,3)))
						<button data-toggle="modal" data-target="#myModal" class="btn btn-success">Submit Approval</button>
						{{-- Modal Submit Approval --}}
						<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
							<div role="document" class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 id="createGroupLabel" class="modal-title">Submit Approval Memo Transaction &raquo; {!! $data['transmemo']->trans_memo_code !!}</h4>
											<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
									</div>
									<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ url('trans/memo-submit-approval') }}">
										<div class="modal-body">
	                                	{!! csrf_field() !!}
	                                		<div class="form-group">
	                                    		<div class="col-md-12">
	                                        		<label for="comment">Comment</label>
	                                        		<input type="text" name="comment" class="form-control form-control-line" data-emojiable="true" value="{{ old('comment') }}">
	                                        		<input type="hidden" name="id" value="{!! $data['transmemo']->id !!}">
	                                        		<input type="hidden" name="status" value="{!! $data['transmemo']->status !!}">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-success">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					{{-- End Modal Create --}}
					@endif
					{{-- End --}}

					{{-- Start - Jika pengguna adalah Approver / Reviewer memo transaction dan status adalah request --}}
					@foreach($data['memoapproval'] as $approver)
						@if($approver->approver == (new \App\Http\permissions)->getNIK_byEmail(Auth::user()->email) && $approver->status == 1)
							<button data-toggle="modal" data-target="#myModal" class="btn btn-success" onclick="isApproveRevise(2)">Approve</button>
							<button data-toggle="modal" data-target="#myModal" class="btn btn-warning" onclick="isApproveRevise(3)">Revise</button>
							<button data-toggle="modal" data-target="#myModal" class="btn btn-danger" onclick="isApproveRevise(4)">Reject</button>
						{{-- Start Modal --}}
							<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
								<div role="document" class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 id="createGroupLabel" class="modal-title"><span id="label"></span> Memo Transaction &raquo; {!! $data['transmemo']->trans_memo_code !!}</h4>
											<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
										</div>
										<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ url('trans/memo-approver-submit') }}">
											<div class="modal-body">
                                			{!! csrf_field() !!}
                                				<div class="form-group">
                                    				<div class="col-md-12">
                                        				<label for="comment">Comment</label>
                                        				<textarea name="comment" class="form-control form-control-line">{{ old('comment') }}</textarea>
                                        				<input type="hidden" name="id" value="{!! $data['transmemo']->id !!}">
                                        				<input type="hidden" name="approver" value="{!! $approver->approver !!}">
                                        				<input type="hidden" name="status" value="" id="status">
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							{{-- End Modals --}}
							<script type="text/javascript">
								function isApproveRevise(value) {
									var label;
									document.getElementById('status').value = value;
									if (value == 2) {
										label = "Approve";
									} else if (value == 3) {
										label = "Revise";
									} else if (value == 4) {
										label = "Reject";
									}

									document.getElementById('label').innerHTML = label;
								}
							</script>
						@endif
					@endforeach
					{{-- End --}}

					@foreach($data['participate'] as $participate)
						@if($participate == (new \App\Http\permissions)->getNIK_byEmail(Auth::user()->email) && $data['transmemo']->status == 1)
						{{-- Start - Jika pengguna adalah yang membuat memo transaction dan status request atau reject --}}
							<button data-toggle="modal" data-target="#myModalComment" class="btn btn-primary">Add Comment</button>
							{{-- Modal Submit Approval --}}
							<div id="myModalComment" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
								<div role="document" class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 id="createGroupLabel" class="modal-title">Comment - Memo Transaction &raquo; {!! $data['transmemo']->trans_memo_code !!}</h4>
											<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
										</div>
										<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ url('trans/memo-comment-submit') }}">
											<div class="modal-body">
	                                		{!! csrf_field() !!}
	                                			<div class="form-group">
													<div class="col-md-12">
	                                        			<label for="comment">To</label>
	                                        			<select name="to" class="form-control form-control-line">
	                                        				<option>-- Choose --</option>
	                                        					@foreach( (new \App\Http\helpers)->removeArrayItem($data['participate'], (new \App\Http\permissions)->getNIK_byEmail(Auth::user()->email)) as $participate)
	                                        						@foreach($data['employee'] as $employee)
	                                        							@if($participate == $employee->number)
	                                        								<option value="{{ $employee->number }}">{{ $employee->fullname }}</option>
	                                        							@endif
	                                        						@endforeach
	                                        					@endforeach
	                                        			</select>
	                                    			</div>
	                                			<div class="form-group">
	                                    			<div class="col-md-12">
	                                        			<label for="comment">Message</label>
	                                        			<textarea name="comment" class="form-control form-control-line">{{ old('comment') }}</textarea>
	                                        			<input type="hidden" name="id" value="{!! $data['transmemo']->id !!}">
	                                        			<input type="hidden" name="status" value="{!! $data['transmemo']->status !!}">
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							{{-- End Modal Create --}}
						@endif
					@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection