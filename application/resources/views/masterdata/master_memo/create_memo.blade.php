@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<form class="form-horizontal form-material" method="post" action="{{route('store-memo')}}">
						{!! csrf_field() !!}
						<div class="card-body">
							
							{{-- Start - Branch --}}
							{{-- <div class="form-group">
								<div class="col-md-12">
									<label for="entity">Entity</label>
									<select name="entity" class="form-control form-control-line select2" id="branch">
										<option value="">-- Choose --</option>
										@foreach($data['branch'] as $branch)
											<option value="{{$branch->id}}">{{$branch->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="division">Division</label>
									<select name="division" class="form-control form-control-line select2" id="division">
										<option value="#">-- Choose --</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="department">Department</label>
									<select name="department" class="form-control form-control-line select2" id="department">
										<option value="#">-- Choose --</option>
									</select>
								</div>
							</div> --}}
							{{-- End - Branch --}}

							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_code">Index (Memo Code)</label>
									<input type="text" name="memo_code" class="form-control form-control-line" value="{{old('memo_code')}}" placeholder="Ex. C.A.1.1">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_name">Name of Document</label>
									<input type="text" name="memo_name" class="form-control form-control-line" value="{{old('memo_name')}}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="remarks">Remarks</label>
									<textarea name="remarks" class="form-control form-control-line">{{old('remarks')}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_type">Type Document</label>
									<select name="memo_type" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['memotype'] as $type)
											<option value="{{$type->id}}" @if(old('memo_type') == $type->id) selected @endif>{{$type->type_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_format">Format Document</label>
									<select name="memo_format" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['memoformat'] as $format)
											<option value="{{$format->id}}" @if(old('memo_format') == $format->id) selected @endif>{{$format->format_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="initiated">Initiated By</label>
									<select name="initiated" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['initiated'] as $initiated)
											<option value="{{$initiated->id}}" @if(old('initiated') == $initiated->id) selected @endif>{{$initiated->initiated_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="loa">LOA</label>
									<select name="loa[]" class="form-control form-control-line select2" id="loa" multiple="multiple">
										@foreach($data['loa'] as $loa)
											<option value="{{$loa->id}}"
												@if(old('loa'))
													{{ (in_array($loa->id, old('loa')) ? "selected":"") }}
												@endif>
												{{$loa->loa_name}}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="loa">Last Approval</label>
									<select name="last_approval" class="form-control form-control-line select2" id="loa">
										@foreach($data['boolean'] as $value)
											<option value="{{$value->param_value}}" @if(old('last_approval') == $value->param_value) selected @endif>{{$value->param_desc}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="is_zone">is Zone?</label>
									<select name="is_zone" class="form-control form-control-line select2" id="is_zone">
										@foreach($data['boolean'] as $value)
											<option value="{{$value->param_value}}" @if(old('is_zone') == $value->param_value) selected @endif>{{$value->param_desc}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<br>
							<div class="col-md-12 text-right">
								<button type="reset" class="btn btn-lg btn-danger">Reset</button>
								<button type="submit" class="btn btn-lg btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$("#branch").change(function(){
			var selectedBranch = $("#branch").val();
			var url = "{{ url('/') }}/api/get-division/"+selectedBranch;
			
			$.get(url, function(data) {
				var division = $("#division");
				division.empty();
				division .append($("<option/>").text("-- Choose --"));
				$.each(data, function(key, value) {   
					division .append($("<option/>").attr("value", value.id).text(value.name));
				});
			});
		});

		$("#division").change(function(){
			var selectedDivision = $("#division").val();
			var url = "{{ url('/') }}/api/get-department/"+selectedDivision;
			
			$.get(url, function(data) {
				var department = $("#department");
				department.empty();
				department .append($("<option/>").text("-- Choose --"));
				$.each(data, function(key, value) {   
					department .append($("<option/>").attr("value", value.id).text(value.name));
				});
			});
		});
	</script>
@endsection