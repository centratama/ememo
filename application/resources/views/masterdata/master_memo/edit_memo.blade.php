@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}} &raquo; {{$data['memo'][0]->memo_name}}</h3>
					</div>
					{{ Form::model($data['memo'], array('route' => ['update-memo', $data['memo'][0]->id], 'method' => 'patch', 'class'=>'form-horizontal form-material')) }}
						{!! csrf_field() !!}
						<div class="card-body">
							{{-- Start - Branch --}}
							{{-- <div class="form-group">
								<div class="col-md-12">
									<label for="entity">Entity</label>
									<select name="entity" class="form-control form-control-line select2" id="branch">
										<option value="">-- Choose --</option>
										@foreach($data['branch'] as $branch)
											<option value="{{$branch->id}}" {{$branch->id == $data['memo'][0]->entity_id ? "selected" : ""}}>{{$branch->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="division">Division</label>
									<select name="division" class="form-control form-control-line select2" id="division" disabled>
										<option value="#">-- Choose --</option>
										@foreach($data['organization'] as $org)
											<option value="{{$org->id}}" {{$org->id == $data['memo'][0]->division_id ? "selected" : ""}}>{{$org->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="department">Department</label>
									<select name="department" class="form-control form-control-line select2" id="department" disabled>
										<option value="#">-- Choose --</option>
										@foreach($data['organization'] as $org)
											<option value="{{$org->id}}" {{$org->id == $data['memo'][0]->dept_id ? "selected" : ""}}>{{$org->name}}</option>
										@endforeach
									</select>
								</div>
							</div> --}}
							{{-- End - Branch --}}

							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_name">Name of Document</label>
									<input type="text" name="memo_name" class="form-control form-control-line" value="{{$data['memo'][0]->memo_name}}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="remarks">Remarks</label>
									<textarea name="remarks" class="form-control form-control-line">{{$data['memo'][0]->remarks}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_type">Type Document</label>
									<select name="memo_type" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['memotype'] as $type)
											<option value="{{$type->id}}" {{$type->id == $data['memo'][0]->memotype_id ? "selected" : ""}}>{{$type->type_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="memo_format">Format Document</label>
									<select name="memo_format" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['memoformat'] as $format)
											<option value="{{$format->id}}" {{$format->id == $data['memo'][0]->memoformat_id ? "selected" : ""}}>{{$format->format_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="initiated">Initiated By</label>
									<select name="initiated" class="form-control form-control-line select2">
										<option value="">-- Choose --</option>
										@foreach($data['initiated'] as $initiated)
											<option value="{{$initiated->id}}" {{$initiated->id == $data['memo'][0]->initiated_id ? "selected" : ""}}>{{$initiated->initiated_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="loa">LOA</label>
									<select name="loa[]" class="form-control form-control-line select2" id="loa" multiple="multiple">
										@foreach($data['loa'] as $loa)
											<option value="{{$loa->id}}"
												@foreach($data['memoloa'] as $memoloa)
													{{$loa->id == $memoloa->id ? "selected" : ""}}
												@endforeach>
												{{$loa->loa_name}}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="loa">Last Approval</label>
									<select name="last_approval" class="form-control form-control-line select2" id="loa">
										@foreach($data['boolean'] as $value)
											<option value="{{$value->param_value}}" {{$value->param_value == $data['memo'][0]->is_last_approval ? "selected" : ""}}>{{$value->param_desc}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label for="is_zone">is Zone?</label>
									<select name="is_zone" class="form-control form-control-line select2" id="is_zone">
										@foreach($data['boolean'] as $value)
											<option value="{{$value->param_value}}" {{$value->param_value == $data['memo'][0]->is_zone ? "selected" : ""}}>{{$value->param_desc}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<br>
							<div class="col-md-12 text-right">
								<button type="reset" class="btn btn-lg btn-danger">Reset</button>
								<button type="submit" class="btn btn-lg btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$("#branch").change(function(){
			var selectedBranch = $("#branch").val();
			var url = "{{ url('/') }}/api/get-division/"+selectedBranch;
			
			$.get(url, function(data) {
				var division = $("#division");
				division.empty();
				division .append($("<option/>").text("-- Choose --"));
				$.each(data, function(key, value) {   
					division .append($("<option/>").attr("value", value.id).text(value.name));
				});
			});

			$("#division").removeAttr("disabled");
		});

		$("#division").change(function(){
			var selectedDivision = $("#division").val();
			var url = "{{ url('/') }}/api/get-department/"+selectedDivision;
			
			$.get(url, function(data) {
				var department = $("#department");
				department.empty();
				department .append($("<option/>").text("-- Choose --"));
				$.each(data, function(key, value) {   
					department .append($("<option/>").attr("value", value.id).text(value.name));
				});
			});

			$("#department").removeAttr("disabled");
		});
	</script>
@endsection