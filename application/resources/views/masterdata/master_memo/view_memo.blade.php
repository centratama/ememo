@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>
							{{ $data['title'] }} &raquo; {{$data['memo'][0]->memo_name}}
						</h3>
					</div>
					<div class="card-body card-block">
						<div class="row">
							{{-- <div class="col-md-2">
								<b>Entity</b>:
							</div>
							<div class="col-md-10">
								{{$data['entity'][0]->name}}
							</div>
							<div class="col-md-2">
								<b>Division</b>:
							</div>
							<div class="col-md-10">
								{{$data['division'][0]->name}}
							</div>
							<div class="col-md-2">
								<b>Department</b>:
							</div>
							<div class="col-md-10">
								{{$data['department'][0]->name}}
							</div> --}}
							<div class="col-md-2">
								<b>Memo Code (Index)</b>:
							</div>
							<div class="col-md-10">
								{{$data['memo'][0]->memo_code}}
							</div>
							<div class="col-md-2">
								<b>Memo Name</b>:
							</div>
							<div class="col-md-10">
								{{$data['memo'][0]->memo_name}}
							</div>
							<div class="col-md-2">
								<b>Memo Type</b>:
							</div>
							<div class="col-md-10">
								{{$data['memotype'][0]->type_name}}
							</div>
							<div class="col-md-2">
								<b>Memo Format</b>:
							</div>
							<div class="col-md-10">
								{{$data['memoformat'][0]->format_name}}
							</div>
							<div class="col-md-2">
								<b>Initiated By</b>:
							</div>
							<div class="col-md-10">
								{{$data['initiated'][0]->initiated_name}}
							</div>
							@foreach($data['loa'] as $key => $loa)
								<div class="col-md-2">
									<b>LOA-{{++$key}}</b>:
								</div>
								<div class="col-md-10">
									{{$loa->loa_name}}
								</div>
							@endforeach
							<div class="col-md-2">
								<b>Last Approval</b>:
							</div>
							<div class="col-md-10">
								{{$data['last_approval']->param_desc}}
							</div>
							<div class="col-md-2">
								<b>is Zone?</b>:
							</div>
							<div class="col-md-10">
								{{$data['is_zone']->param_desc}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection