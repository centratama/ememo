@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					@if ((new \App\Http\permissions)->isAllowed('master_memo_create'))
						<center>
							<a href="{{url('master/memo-create')}}" class="btn btn-info btn-lg text-uppercase waves-effect waves-light"><i class="mdi mdi-plus"></i> Add New Memo</a>
						</center>
					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<div class="card-body table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Memo Name</th>
									<th>Code</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['memo'] as $key => $row)
									<tr>
										<td>{{++$key}}</td>
										<td>{{$row->memo_name}}</td>
										<td>{{$row->memo_code}}</td>
										<td>
											<ul class="list-inline">
												
												@if ((new \App\Http\permissions)->isAllowed('master_memo_view'))
												<li class="list-inline-item">
													<a href="{{url('master/memo-view')}}/{{$row->id}}" alt="View Detail Memo" title="View Detail Memo"> <i class="mdi mdi-information-variant mdi-24px text-info"></i>
													</a>
												</li>
												@endif

												@if ((new \App\Http\permissions)->isAllowed('master_memo_update'))
												<li class="list-inline-item">
													<a href="{{url('master/memo-edit')}}/{{$row->id}}" alt="Change Data Memo" title="Change Data Memo"> <i class="mdi mdi-pencil mdi-24px text-warning"></i>
													</a>


												</li>
												@endif

												@if ((new \App\Http\permissions)->isAllowed('master_memo_delete'))
												<li class="list-inline-item">
													<a href="#" data-toggle="modal" data-target="#myDelete{{$row->id}}" alt="Delete Data Memo" title="Delete Data Memo"> <i class="mdi mdi-delete mdi-24px text-danger"></i>
													</a>
												</li>

												{{-- Start Modal Delete --}}
													<div id="myDelete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myDelete{{$row->id}}Label" aria-hidden="true" class="modal fade text-left">
														<div role="document" class="modal-dialog">
															<div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 id="myDelete{{$row->id}}Label" class="modal-title"> Delete Data Memo </h4>
                                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                                </div>
                                                                {{ Form::model($row, array('route' => ['delete-memo', $row->id], 'method' => 'get', 'class'=>'form-horizontal form-material')) }}
                                                                <div class="modal-body">
                                        							<h3>Do you want to delete {{$row->memo_name}}?</h3>
                                                                </div>
                                                                <div class="modal-footer">
																	<button type="submit" class="btn btn-danger">Yes</button>
																	<button type="button" data-dismiss="modal" class="btn">No</button>
																</div>
                                                                {{ Form::close() }}
                                                            </div>
														</div>
													</div>
												{{-- End Modal Delete --}}
												@endif

											</ul>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection