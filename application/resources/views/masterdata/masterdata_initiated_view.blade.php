@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					{{-- <div class="card-header">
						<h3>
							{{ $data['title'] }}
						</h3>
					</div> --}}
					<div class="card-body">
						<div class="row">
							<div class="col-md-2">
								<b>Initiated Name</b>:
							</div>
							<div class="col-md-3">
								{{ $data['initiated'][0]->initiated_name }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>Initiated Employee</h3>
					</div>
					<div class="card-body">
						{{ Form::model($data['initiated'], array('route' => ['update-initiated-employee', $data['initiated'][0]->id], 'method' => 'patch', 'class'=>'form-horizontal form-material')) }}
							{!! csrf_field() !!}
							<div class="form-group">
								<div class="col-md-12">
									<select name="pic[]" id="pic" class="form-control form-control-line select2" multiple="multiple">
										<option value="">-- Choose --</option>
										@foreach($data['employee'] as $employee)
											<option value="{{$employee->number}}"
												@foreach($data['initiated_employee'] as $initiated)
													{{$employee->number == $initiated->nik ? "selected" : ""}}
												@endforeach
												>{{$employee->fullname}}</option>
										@endforeach
									</select>
									<input type="hidden" name="initiated_id" value="{{ $data['initiated'][0]->id }}">
								</div>
							</div>
							@if ((new \App\Http\permissions)->isAllowed('master_initiated-employee_update'))
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-lg btn-success">Update</button>
							</div>
							@endif
						{{ Form::close() }}
						<div class="col-md-12">
							<ul>
								@foreach($data['employee'] as $employee)
									@foreach($data['initiated_employee'] as $initiated)
										{!! $employee->number == $initiated->nik ? "<li>$employee->fullname</li>" : ""!!}
									@endforeach
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection