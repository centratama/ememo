@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					@if ((new \App\Http\permissions)->isAllowed('master_memo-format_create'))
						<center>
							<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-lg text-uppercase waves-effect waves-light"><i class="mdi mdi-plus"></i> Add New Memo Format</button>
						</center>
						{{-- Modal Create --}}
						<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
							<div role="document" class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 id="createGroupLabel" class="modal-title">Add New Format Memo</h4>
											<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
									</div>
									<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ url('master/memo-format-create') }}">
									<div class="modal-body">
                                    	{!! csrf_field() !!}
                                    	<div class="form-group">
                                        	<div class="col-md-12">
                                        		<label for="format_name">Format Name</label>
                                            	<input type="text" class="form-control form-control-line" name="format_name" id="format_name" value="{{old('format_name')}}">
                                        	</div>
                                    	</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-success">Submit</button>
									</div>
									</form>
								</div>
							</div>
						</div>
						{{-- End Modal Create --}}


					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<div class="card-body table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Format Name</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['memoformat'] as $key => $row)
									<tr>
										<td>{{++$key}}.</td>
										<td>{{$row->format_name}}</td>
										<td>
											<ul class="list-inline">
												@if ((new \App\Http\permissions)->isAllowed('master_memo-format_update'))
												<li class="list-inline-item">
													<a href="#" data-toggle="modal" data-target="#myEdit{{$row->id}}" alt="Change Data Memo Format" title="Change Data Memo Format"> <i class="mdi mdi-pencil mdi-24px text-warning" aria-hidden="true"></i>
													</a>

													{{-- Start Modal Edit --}}
													<div id="myEdit{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myEdit{{$row->id}}Label" aria-hidden="true" class="modal fade text-left">
														<div role="document" class="modal-dialog">
															<div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 id="myEdit{{$row->id}}Label" class="modal-title"> Change Data Memo Format &raquo; {{$row->format_name}}</h4>
                                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                                </div>
                                                                {{ Form::model($row, array('route' => ['update-memoformat', $row->id], 'method' => 'patch', 'class'=>'form-horizontal form-material')) }}
                                                                <div class="modal-body">
                                                                	<div class="form-group">
                                        								<div class="col-md-12">
                                        									<label for="format_name">Format Name</label>
                                            								<input type="text" class="form-control form-control-line" name="format_name" id="format_name" value="{{$row->format_name}}">
                                            								<input type="hidden" name="id" value="{{$row->id}}">
                                        								</div>
                                    								</div>
                                                                </div>
                                                                <div class="modal-footer">
																	<button type="submit" class="btn btn-success">Submit</button>
																</div>
                                                                {{ Form::close() }}
                                                            </div>
														</div>
													</div>
													{{-- End Modal Edit --}}

												</li>
												@endif

												@if ((new \App\Http\permissions)->isAllowed('master_memo-format_delete'))
												<li class="list-inline-item">
													<a href="#" data-toggle="modal" data-target="#myDelete{{$row->id}}" alt="Delete Data Memo Format" title="Delete Data Memo Format"> <i class="mdi mdi-delete mdi-24px text-danger" aria-hidden="true"></i>
													</a>

													{{-- Start Modal Delete --}}
													<div id="myDelete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myDelete{{$row->id}}Label" aria-hidden="true" class="modal fade text-left">
														<div role="document" class="modal-dialog">
															<div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 id="myDelete{{$row->id}}Label" class="modal-title"> Delete Data Memo Format &raquo; {{$row->format_name}}</h4>
                                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                                </div>
                                                                {{ Form::model($row, array('route' => ['delete-memoformat', $row->id], 'method' => 'get', 'class'=>'form-horizontal form-material')) }}
                                                                <div class="modal-body">
                                        							<h3>Do you want to delete {{$row->format_name}}?</h3>
                                                                </div>
                                                                <div class="modal-footer">
																	<button type="submit" class="btn btn-danger">Yes</button>
																	<button type="button" data-dismiss="modal" class="btn">No</button>
																</div>
                                                                {{ Form::close() }}
                                                            </div>
														</div>
													</div>
													{{-- End Modal Delete --}}
												</li>
												@endif
											</ul>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection