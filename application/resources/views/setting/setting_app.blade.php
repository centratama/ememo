@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<div class="card-body">
						<form class="form-horizontal form-material">
							{!! csrf_field() !!}
							@foreach($data['setting'] as $setting)
							<div class="form-group">
								<div class="col-md-12">
									<label for="{!! $setting->name !!}">{!! $setting->label !!}</label>
									{!! (new \App\Http\helpers)->getInputType($setting->name, $setting->value, $setting->type_input, $setting->dataenum) !!}
								</div>
							</div>
							@endforeach
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection