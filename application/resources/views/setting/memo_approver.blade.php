@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>{{$data['title']}}</h3>
					</div>
					<div class="card-body table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Memo Name</th>
									<th>Memo Code</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['memo'] as $key => $row)
									<tr>
										<td>{{++$key}}</td>
										<td>{{$row->memo_name}}</td>
										<td>{{$row->memo_code}}</td>
										<td>
											<ul class="list-inline">
												@if ((new \App\Http\permissions)->isAllowed('setting_memo-approver_view'))
												<li class="list-inline-item">
													<a href="{{ url('setting/memo-approver-view') }}/{{$row->id}}" alt="View Detail Initiated" title="View Detail Initiated"> <i class="mdi mdi-information-variant mdi-24px text-info"></i>
													</a>
												</li>
												@endif
											</ul>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection