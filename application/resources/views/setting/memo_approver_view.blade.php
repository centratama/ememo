@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-body">
						<div class="row">
							<div class="col-md-2">
								<b>Memo Name</b>:
							</div>
							<div class="col-md-6">
								{{ $data['memo'][0]->memo_name }}
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<b>Memo Code</b>:
							</div>
							<div class="col-md-6">
								{{ $data['memo'][0]->memo_code }}
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<b>Remarks</b>:
							</div>
							<div class="col-md-6">
								{{ $data['memo'][0]->remarks }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					@if ((new \App\Http\permissions)->isAllowed('setting_memo-approver_create'))
						<center>
							<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-lg text-uppercase waves-effect waves-light"><i class="mdi mdi-plus"></i> Add New Approver</button>
						</center>
						{{-- Modal Create --}}
						<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">
							<div role="document" class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 id="createGroupLabel" class="modal-title">Add New Initiated</h4>
											<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
									</div>
									<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ url('setting/memo-approver-create') }}">
									<div class="modal-body">
                                    	{!! csrf_field() !!}
                                    	<div class="form-group">
                                        	<div class="col-md-12">
                                        		<label for="approval_type">Approval Type</label>
                                            	<select id="approval_type" name="approval_type" class="form-control form-control-line">
                                            		<option>-- Choose --</option>
                                            		@foreach($data['approvaltype'] as $approval)
                                            		<option value="{{$approval->id}}" {{old('approval_type') == $approval->id ? "selected" : ""}}>{{$approval->approval_name}}</option>
                                            		@endforeach
                                            	</select>
                                        	</div>
                                    	</div>
                                    	<div class="form-group">
                                        	<div class="col-md-12">
                                        		<label for="employee">Employee Name</label>
                                            	<select id="employee" name="employee" class="form-control form-control-line">
                                            		<option>-- Choose --</option>
                                            		@foreach($data['employee'] as $employee)
                                            		<option value="{{$employee->number}}" {{old('employee') == $employee->number ? "selected" : ""}}>{{$employee->fullname}}</option>
                                            		@endforeach
                                            	</select>
                                        	</div>
                                    	</div>
                                    	<div class="form-group">
                                        	<div class="col-md-12">
                                        		<label for="order">Order</label>
                                            	<input type="number" name="order" id="order"  class="form-control form-control-line" value="{{old('order')}}">
                                        	</div>
                                    	</div>
									</div>
									<input type="hidden" name="memo_id" value="{{ $data['memo'][0]->id }}">
									<div class="modal-footer">
										<button type="submit" class="btn btn-success">Submit</button>
									</div>
									</form>
								</div>
							</div>
						</div>
						{{-- End Modal Create --}}


					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-block">
					<div class="card-header">
						<h3>List Approver Memo &raquo; {{ $data['memo'][0]->memo_name }}</h3>
					</div>
					<div class="card-body table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Approval Type</th>
									<th>Approver</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@if($data['approver']->count() > 0)
									@foreach($data['approver'] as $key => $row)
										<tr>
											<td>{!! ++$key !!}.</td>
											<td>
												@foreach($data['approvaltype'] as $approval)
												{!! $approval->id == $row->approval_id ? $approval->approval_name : "" !!}
												@endforeach
											</td>
											<td>
												@foreach($data['employee'] as $employee)
												{!! $employee->number == $row->nik ? $employee->fullname : "" !!}
												@endforeach
											</td>
											<td>
												@if ($row->approval_id != 99)
												<ul class="list-inline">
													@if ((new \App\Http\permissions)->isAllowed('setting_memo-approver_update'))
													<li class="list-inline-item">
														<a href="#" data-toggle="modal" data-target="#myEdit{{$row->id}}" alt="Change Data Approver" title="Change Data Approver"> <i class="mdi mdi-pencil mdi-24px text-warning" aria-hidden="true"></i>
														</a>

														{{-- Start Modal Edit --}}
														<div id="myEdit{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myEdit{{$row->id}}Label" aria-hidden="true" class="modal fade text-left">
															<div role="document" class="modal-dialog">
																<div class="modal-content">
	                                                                <div class="modal-header">
	                                                                    <h4 id="myEdit{{$row->id}}Label" class="modal-title"> Change Data Approver</h4>
	                                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
	                                                                </div>
	                                                                {{ Form::model($row, array('route' => ['update-memo-approver', $row->id], 'method' => 'patch', 'class'=>'form-horizontal form-material')) }}
	                                                                <div class="modal-body">
	                                                                	<div class="form-group">
								                                        	<div class="col-md-12">
								                                        		<label for="approval_type">Approval Type</label>
								                                            	<select id="approval_type" name="approval_type" class="form-control form-control-line">
								                                            		<option>-- Choose --</option>
								                                            		@foreach($data['approvaltype'] as $approval)
								                                            		<option value="{{$approval->id}}" {{$row->approval_id == $approval->id ? "selected" : ""}}>{{$approval->approval_name}}</option>
								                                            		@endforeach
								                                            	</select>
								                                        	</div>
								                                    	</div>
								                                    	<div class="form-group">
								                                        	<div class="col-md-12">
								                                        		<label for="employee">Employee Name</label>
								                                            	<select id="employee" name="employee" class="form-control form-control-line">
								                                            		<option>-- Choose --</option>
								                                            		@foreach($data['employee'] as $employee)
								                                            		<option value="{{$employee->number}}" {{$row->nik == $employee->number ? "selected" : ""}}>{{$employee->fullname}}</option>
								                                            		@endforeach
								                                            	</select>
								                                        	</div>
								                                    	</div>
								                                    	<div class="form-group">
								                                        	<div class="col-md-12">
								                                        		<label for="order">Order</label>
								                                            	<input type="number" name="order" id="order"  class="form-control form-control-line" value="{{$row->order}}">
								                                        	</div>
								                                    	</div>
								                                    	<input type="hidden" name="memo_id" value="{{ $data['memo'][0]->id }}">
	                                                                </div>
	                                                                <div class="modal-footer">
																		<button type="submit" class="btn btn-success">Submit</button>
																	</div>
	                                                                {{ Form::close() }}
	                                                            </div>
															</div>
														</div>
														{{-- End Modal Edit --}}

													</li>
													@endif

													@if ((new \App\Http\permissions)->isAllowed('setting_memo-approver_delete'))
													<li class="list-inline-item">
														<a href="#" data-toggle="modal" data-target="#myDelete{{$row->id}}" alt="Delete Data Approver" title="Delete Data Approver"> <i class="mdi mdi-delete mdi-24px text-danger" aria-hidden="true"></i>
														</a>

														{{-- Start Modal Delete --}}
														<div id="myDelete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myDelete{{$row->id}}Label" aria-hidden="true" class="modal fade text-left">
															<div role="document" class="modal-dialog">
																<div class="modal-content">
	                                                                <div class="modal-header">
	                                                                    <h4 id="myDelete{{$row->id}}Label" class="modal-title"> Delete Data Approver</h4>
	                                                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
	                                                                </div>
	                                                                {{ Form::model($row, array('route' => ['delete-memo-approver', $row->id], 'method' => 'get', 'class'=>'form-horizontal form-material')) }}
	                                                                <div class="modal-body">
	                                        							<h3>Do you want to delete this data?</h3>
	                                                                </div>
	                                                                <div class="modal-footer">
																		<button type="submit" class="btn btn-danger">Yes</button>
																		<button type="button" data-dismiss="modal" class="btn">No</button>
																	</div>
	                                                                {{ Form::close() }}
	                                                            </div>
															</div>
														</div>
														{{-- End Modal Delete --}}
													</li>
													@endif

												</ul>
												@endif
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="4">Data is not available.</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection