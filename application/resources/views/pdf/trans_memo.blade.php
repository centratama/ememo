@extends('layouts.pdf_layout')
<title>{!! $data['transmemo']->trans_memo_code !!}</title>
@section('content')
	<div class="col-lg-12">
		
		{{-- Header --}}
		<img src="{{url('/')}}/assets/logo-3-entitas.png"/>
		<h4>Memo</h4>
		<div style="border-bottom: 3px solid #000000;"></div>
		<br>

		{{-- Body --}}
		<table class="table" style="font-size: 11px;">
			<tr>
				<th width="10px">No.</th>
				<td width="2px">:</td>
				<td width="">{!! $data['transmemo']->trans_memo_code !!}</td>

				<th width="150px">No. Temp. MEMO</th>
				<td width="2px">:</td>
				<td>{!! $data['memo']->memo_code !!}</td>
			</tr>
			<tr>
				<th>Date</th>
				<td width="2px">:</td>
				<td>{!! date("d M, Y H:i", strtotime($data['transmemo']->initiated_date)) !!}</td>

				<th>MEMO Type</th>
				<td width="2px">:</td>
				<td>{!! $data['memo']->memo_name !!}</td>
			</tr>

			@foreach($data['memoloa'] as $x => $memoloa)
				<tr>
					<td colspan="3"></td>

					<th>Desc. Level Of Authority - {{++$x}}</th>
					<td width="2px">:</td>
					<td>{{$memoloa->loa_name}}</td>
				</tr>
			@endforeach

			<tr>
				<th>To</th>
				<td width="2px">:</td>
				<td colspan="4">
					@foreach($data['memoapproval'] as $key => $approval)
						@foreach($data['employee'] as $employee)
							@if ($employee->number == $approval->approver)
								{{++$key}}. <b>{!! $employee->fullname !!}</b> ({!! $employee->job !!})<br>
							@endif
						@endforeach
					@endforeach
				</td>
			</tr>
			{{-- <tr>
				<td colspan="6"></td>
			</tr> --}}
			<tr>
				<th colspan="6">From</th>
			</tr>

			<tr>
				<th width="10px">NIK</th>
				<td width="2px">:</td>
				<td width="">{!! $data['created']->number !!}</td>

				<th width="150px">Department</th>
				<td width="2px">:</td>
				<td>{!! $data['created']->namelevel3 == NULL ? $data['created']->namelevel1 : $data['created']->namelevel2 !!}</td>
			</tr>
			<tr>
				<th width="10px">Name</th>
				<td width="2px">:</td>
				<td width="">{!! $data['created']->fullname !!}</td>

				<th width="150px">Division</th>
				<td width="2px">:</td>
				<td>{!! $data['created']->namelevel3 == NULL ? $data['created']->namelevel2 : $data['created']->namelevel3 !!}</td>
			</tr>
			<tr>
				<th width="10px">Title</th>
				<td width="2px">:</td>
				<td width="" colspan="4">{!! $data['created']->job !!}</td>

				{{-- <th width="150px">Operator</th>
				<td width="2px">:</td>
				<td></td> --}}
			</tr>
		</table>
		<div style="border-bottom: 3px solid #000000;"></div>
		<table class="table" style="border-width: thin; text-align: center;">
			<tr>
				@foreach($data['memochar'] as $char)
					<td><img src="{!! $char->id == $data['transmemo']->char_id ? Config::get('constants.options.imgcheck') : Config::get('constants.options.imgnocheck')!!}"> {!! $char->char_name !!}</td>
				@endforeach
			</tr>
		</table>
		<div style="border-bottom: 3px solid #000000;"></div>

		<table class="table page-break" border="0">
			<tr>
				<th>A. Background</th>
			</tr>
			<tr style="background-color: #BBDEFB">
				<td>{!! $data['transmemo']->background !!}</td>
			</tr>
			<tr>
				<th>B. Consideration</th>
			</tr>
			<tr style="background-color: #BBDEFB">
				<td>{!! $data['transmemo']->consideration !!}</td>
			</tr>
			<tr>
				<th>C. Points to be Approved</th>
			</tr>
			<tr style="background-color: #BBDEFB">
				<td>{!! $data['transmemo']->points_to_be_app !!}</td>
			</tr>

		</table>

		<table class="table">
			<tr>
				<th colspan="6">APPROVAL AND AUTHORITY TO PROCEED</th>
			</tr>
			<tr>
				<td colspan="6">We approve MeMo as described above, and authorize the team to proceed.</td>
			</tr>

			<tr>
				<th></th>
				<th>Name</th>
				<th>Title</th>
				<th>Date</th>
				<th>Comments</th>
				<th>Status</th>
			</tr>
			<tr>
				<td>Initiated</td>
				<td>{!! $data['created']->fullname !!}</td>
				<td>{!! $data['created']->job !!}</td>
				<td>{!! date("d M, Y H:i", strtotime($data['transmemo']->initiated_date)) !!}</td>
				<td>{!! $data['transmemo']->comment !!}</td>
				<td>Submited</td>
			</tr>
			@foreach($data['memoapproval'] as $key => $approval)
				<tr>
					<td>
						@foreach($data['approvaltype'] as $type)
							{!! $approval->approval_id == $type->id ? $type->approval_name : "" !!}
						@endforeach
					</td>
					<td>
						@foreach($data['employee'] as $employee)
							{!! $approval->approver == $employee->number ? $employee->fullname."</td><td>". $employee->job : "" !!}
						@endforeach
					</td>
					<td>{!! date("d M, Y H:i", strtotime($approval->dateInitiated)) !!}</td>
					<td>{!! $approval->comment !!}</td>
					<td>
						@foreach($data['memostatus'] as $param)
							{!! $approval->status == $param->param_value ? $param->param_desc : "" !!}
						@endforeach
					</td>
				</tr>
			@endforeach
		</table>
@endsection