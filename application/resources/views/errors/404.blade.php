<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/assets/logo2.jpg">
    <title>404 - Page Not Found</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('assets/material-lite/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('assets/material-lite/lite/css/style.css') !!}
    <!-- You can change the theme colors from here -->
    {!! Html::style('assets/material-lite/lite/css/colors/blue.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>404</h1>
                <h3 class="text-uppercase">Page Not Found !</h3>
                <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                <a href="{{ URL::previous() }}" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
            <footer class="footer text-center"> eMemo 0.1 © {{date('Y')}} <a href="https://www.centratamagroup.com">Centratama Group</a> </footer>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    {!! Html::script('assets/material-lite/assets/plugins/jquery/jquery.min.js') !!}
    <!-- Bootstrap tether Core JavaScript -->
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/tether.min.js') !!}
    {!! Html::script('assets/material-lite/assets/plugins/bootstrap/js/bootstrap.min.js') !!}
    <!--Wave Effects -->
    {!! Html::script('assets/material-lite/lite/js/waves.js') !!}
</body>

</html>
