<?php

$variables['default'] = array(
	'no_permission'                  => false,
    'admin_group'                    => 'Admin',
    'default_group'                  => 'default',
    'public_group'                   => 'public',
    'pmo_group'                   	 => 'PMO',
    'db_profile'                     => 'default',
    'users'                          => 'users',
    'groups'                         => 'master_group',
    'group_to_group'                 => 'group_to_group',
    'user_group'					 => 'user_group',
    'permission_menu'                => 'permission_menu',
    'group_permission'               => 'group_permission',
    'user_permission'                => 'user_permission',
);

return $variables;