<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Memo extends Model
{
    //
    public function getByCreated($nik = false)
    {
    	if ($nik === false) {
    		$data = DB::table('trans_memo')
                    ->orderBy('status', 'asc');
    	} else {
    		$data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
    				->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik);
                    })
                    ->select('a.*')
                    ->orderBy('a.status', 'asc');
    	}

    	return $data;
    }

    public function insertApproval($trans_memo_id, $memo_id)
    {
        $now = date("Y-m-d H:i:s");
        $sql = "INSERT INTO trans_memo_approval (trans_memo_id, approval_id, approver, created_at)
                SELECT '{$trans_memo_id}', approval_id, nik, '{$now}'
                FROM memo_approver
                WHERE memo_id = '{$memo_id}'
                ORDER BY approval_id;";

        DB::INSERT($sql);

        return true;
    }

    public function getTransMemo($trans_memo_id)
    {
        $data = DB::table('trans_memo')
                ->where('id', $trans_memo_id);

        return $data;
    }

    public function getTransMemoApproval($trans_memo_id)
    {
        $data = DB::table('trans_memo_approval')
                ->where('trans_memo_id', $trans_memo_id);

        return $data;
    }

    public function updateData($where, $data, $table)
    {
        DB::table($table)->where($where)->update($data);

        return true;
    }

    public function getOneTransMemoApproval($trans_memo_id)
    {
        $where = [
            'trans_memo_id' => $trans_memo_id,
            'status' => 0
        ];
        $data = DB::table('trans_memo_approval')
                ->where($where)
                ->limit(1)
                ->get();

        return $data;

    }

    public function getTransMemoComment($trans_memo_id)
    {
        $data = DB::table('trans_memo_comment')
                ->where('trans_memo_id', $trans_memo_id)
                ->orderBy('created_at', 'asc');

        return $data;
    }

    public function insertApprovalZone($trans_memo_id, $initiator)
    {
        $getInitiator = DB::table('initiated_approver_zone')
                        ->where('nik', '=', $initiator)
                        ->first();

        $now = date("Y-m-d H:i:s");
        if (!empty($getInitiator)) {
            $sql = "INSERT INTO trans_memo_approval (trans_memo_id, approval_id, approver, created_at)
                SELECT '{$trans_memo_id}', approval_id, nik, '{$now}'
                FROM initiated_approver_zone
                WHERE zone = '{$getInitiator->zone}'
                AND approval_id > 0
                ORDER BY approval_id, `order`;";

            DB::INSERT($sql);
        }
        
        return true;
    }

    public function sumDraft($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo')->where('status', 0);
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where('a.status', 0)
                    ->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik)
                        ->orWhere('b.approver', $nik);
                    })
                    ->select('a.*');
        }

        return $data;
    }

    public function sumRequest($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo')->where('status', 1);
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where('a.status', 1)
                    ->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik)
                        ->orWhere('b.approver', $nik);
                    })
                    ->select('a.*');
        }

        return $data;
    }

    public function sumApprove($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo')->where('status', 2);
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where('a.status', 2)
                    ->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik)
                        ->orWhere('b.approver', $nik);
                    })
                    ->select('a.*');
        }

        return $data;
    }

    public function sumRevise($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo')->where('status', 3);
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where('a.status', 3)
                    ->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik)
                        ->orWhere('b.approver', $nik);
                    })
                    ->select('a.*');
        }

        return $data;
    }

    public function sumReject($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo')->where('status', 4);
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where('a.status', 4)
                    ->where(function ($query) use ($nik) {
                        $query->where('a.created_by', $nik)
                        ->orWhere('b.approver', $nik);
                    })
                    ->select('a.*');
        }

        return $data;
    }

    public function getTransMemoApprovalAbove($trans_memo_id)
    {
        $data = DB::table('trans_memo_approval')
                ->where('trans_memo_id', $trans_memo_id)
                ->where('status', 2);

        return $data;
    }

    public function getByApprover($nik = false)
    {
        if ($nik === false) {
            $data = DB::table('trans_memo');
        } else {
            $data = DB::table('trans_memo as a')
                    ->join('trans_memo_approval as b', 'a.id', '=', 'b.trans_memo_id')
                    ->where(function ($query) use ($nik) {
                        $query->where('b.approver', $nik);
                    })
                    ->where('a.status', '>', 0)
                    ->select('a.*')
                    ->orderBy('a.status', 'asc');
        }

        return $data;
    }
}
