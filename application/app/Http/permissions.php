<?php

namespace App\Http;

use DB;
use Auth;
// use App\Http\helpers;
/**
* 
*/
class permissions
{
	
	function __construct()
	{
		//??????????????
		$this->config_vars = config('variables')['default'];
	}

	function isAdmin($user_id = false)
	{
		return $this->isMember($this->config_vars['admin_group'], $user_id);
	}

	function isAllowed($perm_param, $user_id = false)
	{
		//jika user
		if ($user_id == false) {
			$user_id = Auth::user()->id;
		}

		//jika admin
		if ($this->isAdmin($user_id)) {
			return true;
			//mengembalikan nilai true
		}

		//mendapatkan id ketika nama sama dengan $perm_param
		$perm_id = $this->get_perm_id($perm_param);

		//mengambil data dari table $perm_param
		$query = DB::table($this->config_vars['user_permission'])
					->where('perm_id', $perm_id)
					->where('user_id', $user_id)->get();

		//jika jumlah lebih dari 0
		if (count($query) > 0) {
			return true;
		} 

		//jika sama dengan 0
		else {
			$g_allowed = false;
			foreach ($this->get_user_groups($user_id) as $group) {
				if ($this->is_group_allowed($perm_id, $group->name)) {
					$g_allowed = true;
					break;
				}
			}
		return $g_allowed;
		}

	}

	//tested
    /**
     * Is member
     * Check if current user is a member of a group
     * @param int|string $group_par Group id or name to check
     * @param int|bool $user_id User id, if not given current user
     * @return bool
     */
	function isMember($group_param, $user_id = false)
	{
		// if user_id FALSE (not given), current user
        if (!$user_id) {
            $user_id = Auth::user()->id;
		}

		$group_id = $this->get_group_id($group_param);
		
		$query = DB::table($this->config_vars['user_group'])
					->where('user_id', $user_id)
					->where('group_id', $group_id)->get();

		if (count($query) > 0) {
			return true;
		} else {
			return false;
		}
	}

	//tested
    /**
     * Get group id
     * Get group id from group name or id ( ! Case sensitive)
     * @param int|string $group_par Group id or name to get
     * @return int Group id
     */
    public function get_group_id($group_param)
	{
        if (is_numeric ($group_param)) {
			return $group_par;
		}

		$query = DB::table($this->config_vars['groups'])
					->where('name', $group_param)->get();

		if (count($query) == 0){
            return false;
		}
		
		return $query[0]->id;
	}

	//tested
    /**
     * Get permission id
     * Get permission id from permisison name or id
     * @param int|string $perm_par Permission id or name to get
     * @return int Permission id or NULL if perm does not exist
     */
    public function get_perm_id($perm_param)
	{
        if (is_numeric ($perm_param))
            {
            return $perm_param;
            }

        $query = DB::table($this->config_vars['permission_menu'])
        			->where('name', $perm_param)
        			->select('id')->get();

        if (count($query) == 0)
            {
            return false;
            }

        return $query[0]->id;
	}

	/**
     * Get user groups
     * Get groups a user is in
     * @param int|bool $user_id User id to get or FALSE for current user
     * @return array Groups
     */
	public function get_user_groups($user_id = false)
	{
		//jika $user_id tidak tersedia
		if (!$user_id) {
			$user_id = Auth::user()->id;
		}

		//jika $user_id tidak tersedia
		if (!$user_id) {
			$query = DB::table($this->config_vars['groups'])
						->where('name', $this->config_vars['public_group'])->get();
		}

		//jika $user_id tersedia
		elseif ($user_id) {
			$query = DB::table($this->config_vars['user_group'])
						->join($this->config_vars['groups'], 'id', '=', 'group_id')
						->where('user_id', $user_id)->get();
		}

		return $query;
	}

	/**
     * Is Group allowed
     * Check if group is allowed to do specified action, admin always allowed
     * @param int $perm_par Permission id or name to check
     * @param int|string|bool $group_par Group id or name to check, or if FALSE checks all user groups
     * @return bool
     */
	public function is_group_allowed($perm_param, $group_param = false)
	{
		$perm_id = $this->get_perm_id($perm_param);

		// If group param is given
		if ($group_param != false) {
			if (strcasecmp($group_param, $this->config_vars['admin_group']) == 0) {
				return true;
			}

			$group_id = $this->get_group_id($group_param);
			DB::enableQueryLog();
			$query = DB::table($this->config_vars['group_permission'])
						->where('perm_id', $perm_id)
						->where('group_id', $group_id)->get();
			
			$g_allowed = false;

			if (count($query) > 0) {
				$g_allowed = true;
			}
		return $g_allowed;
		}

		else {
			if ($this->isAdmin(Auth::user()->id) || ($this->is_group_allowed($perm_id, $this->config_vars['public_group']))) {
				return true;
			}

			$group_pars = $this->get_user_groups();
			foreach ($group_pars as $g) {
				if ($this->is_group_allowed($perm_id, $g->id)) {
					return true;
				}
			}
		return false;		
		}
	}

	public function getNIK_byEmail($email_user = NULL)
	{
		if ($email_user == NULL) {
			$email_user = Auth::user()->email;
		}

		$data = DB::connection('mysql2')
    			->table('employee')
    			->where('email', $email_user)
    			->first();

    	if (empty($data)) {
    		return null;
    	} else {
    		return $data->number;
    	}
	}

	public function isAllowedReply($array_participate)
	{
		$status = false;
		$nik_user = $this->getNIK_byEmail(Auth::user()->email);
		if (in_array($nik_user, $array_participate)) {
			$status = true;
		}

		return $status;
	}

	public function getName_byNIK($nik)
	{
		$data = DB::connection('mysql2')
    			->table('employee')
    			->where('number', $nik)
    			->first();

    	if (empty($data)) {
    		return null;
    	} else {
    		return $data->fullname;
    	}
	}
}