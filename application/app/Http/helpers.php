<?php
namespace App\Http;
// use App\Menu;
use DB;
use App\Http\permissions;
use App\Masterdata;
use App\Memo;

/**
* 
*/
class helpers
{
	
	function __construct()
	{
		$this->permissions = new permissions;
		$this->Masterdata = new Masterdata;
		$this->Memo = new Memo;
	}

	function getMenus($parent)
	{
		$getMenu = DB::table('master_menu')
					->where('parent_menu', $parent)
					->where('menu_status', 1)
					->orderBy('sort', 'ASC')->get();

		$menu = '';
		foreach ($getMenu as $key => $value) {
			if ($value->menu_idtag == "") {
				if ($this->permissions->isAllowed($value->menu_perms)) {
					$link = url('/').'/'.$value->link;
					$menu .= '<li> <a class="waves-effect waves-dark" href="'. $link .'">'. $value->menu_name .'</a>';
				}
			} else {
				if ($this->permissions->isAllowed($value->menu_perms)) {
					$menu .= '<li> <a class="has-arrow waves-effect waves-dark" href="#' . $value->menu_idtag . '" aria-expanded="false" data-toggle="collapse">' . $value->menu_name . '</a>';
				}
			}

			if ($this->permissions->isAllowed($value->menu_perms)) {
				 $menu .= '<ul id="' . $value->menu_idtag . '" class="collapse list-unstyled">' . $this->getMenus($value->menu_id) . '</ul>';
			}
			$menu .= '</li>';
		}

		return $menu;
	}

	function getMemoCode()
	{
		$countMemo = DB::table('master_memo')->count();

		return "memo_".++$countMemo;
	}

	function getGroup_byUserId($user_id)
	{
		$data = DB::table('user_group as a')
				->join('master_group as b', 'a.group_id', '=', 'b.id')
				->where('a.user_id', $user_id)
				->first();

		if (empty($data)) {
			return NULL;
		} else {
			return $data->name;
		}
	}

	function getMemoTransId()
	{
		$count = DB::table('trans_memo')->count();

		switch (strlen($count)) {
			case 1:
				$seq = '0000'.++$count;
				break;
			case 2:
				$seq = '000'.++$count;
				break;
			case 3:
				$seq = '00'.++$count;
				break;
			case 4:
				$seq = '0'.++$count;
				break;
			case 5:
				$seq = ++$count;
				break;
			default:
				$seq = ++$count;
				break;
		}

		$date = date("Ymd");

		$id = "MEMO-".$date."-".$seq;

		return $id;
	}

	function getAttachmentMemo($attachment, $trans_memo_id)
	{
		$find = ",";
		$attach = "";
		if (strpos($attachment, $find) !== FALSE) {
			$explodeAttach = explode(",", $attachment);
			$attach .= "<ul>";
			foreach ($explodeAttach as $key => $value) {
				$explodeLink = explode("/".$trans_memo_id."/", $value);
				$attach .= "<li>";
				$attach .= "<a target='_blank' href=".$value.">".$explodeLink[1]."</a>";
				$attach .= "</li>";
			}
			$attach .= "</ul>";
		} else {
			$explodeLink = explode("/".$trans_memo_id."/", $attachment);
			$attach = "<ul>";
			$attach .= "<li>";
			$attach .= "<a target='_blank' href=".$attachment.">".$explodeLink[1]."</a>";
			$attach .= "</li>";
			$attach .= "</ul>";
		}

		return $attach;
	}

	function removeArrayItem($array, $item)
	{
		$index = array_search($item, $array);
		if ( $index !== false ) {
			unset( $array[$index] );
		}

		return $array;	
	}

	function getParticipateMemo($trans_memo_id)
	{
		//
        $memoapproval = $this->Memo->getTransMemoApproval($trans_memo_id)->get();
        $memo = $this->Memo->getTransMemo($trans_memo_id)->first();
        $participate = [];
        foreach ($memoapproval as $key => $approval) {
            $participate[$key] = $approval->approver;
        }
        array_push($participate, $memo->created_by);
        //

        return $participate;
	}

	function getComment($trans_memo_id, $parent, $status_memo)
	{
		$data = DB::table('trans_memo_comment')
                ->where('trans_memo_id', $trans_memo_id)
                ->where('parent_id', $parent)
                ->orderBy('created_at', 'asc')->get();
        
        $employees = $this->Masterdata->getEmployee()->get();

        $comment = '';

        foreach ($data as $key => $value) {
        	$comment .= "<article class='timeline-entry'>";
        	$comment .= "<div class='timeline-entry-inner'>";
		    $comment .= "<div class='timeline-icon bg-success'>";
			$comment .= "<i class='entypo-feather'></i>";
			$comment .= "</div>";
			$comment .= "<div class='timeline-label'>";
			$comment .= "<blockquote><b>";
			foreach ($employees as $key => $employee) {
				if ($employee->number == $value->from) {
					$comment .= $employee->fullname;
				}
			}
			$comment .= ":</b> ";
			foreach ($employees as $key => $employee) {
				if ($employee->number == $value->to) {
					$comment .= "@".$employee->fullname." ";
				}
			}
			$comment .= $value->message."<br>";
			$comment .= date("d M, Y H:i", strtotime($value->created_at));
			
			if($this->permissions->isAllowedReply($this->getParticipateMemo($trans_memo_id))){
				if (in_array($status_memo, array(1,3))) {
					$comment .= " &nbsp;&nbsp;&nbsp;";
					$comment .= "<a href='#' data-toggle='modal' data-target='#myReply".$value->id."' title='Reply'>Reply</a>";	
				}
			}
			$comment .= $this->getComment($trans_memo_id, $value->id, $status_memo);
			$comment .= "</blockquote>";
			$comment .= "</div>";
			$comment .= "</div>";
        	$comment .= "</article>";
        }
        return $comment;
	}

	function getReplyCommentModal($trans_memo_id)
	{
		$data = DB::table('trans_memo_comment')
                ->where('trans_memo_id', $trans_memo_id)
                ->orderBy('created_at', 'asc')->get();
		$employees = $this->Masterdata->getEmployee()->get();
		$url = url('trans/memo-reply-comment');

		$modals = '';
		foreach ($data as $key => $value) {
			$modals .= '<div id="myReply'.$value->id.'" tabindex="-1" role="dialog" aria-labelledby="createFormatLabel" aria-hidden="true" class="modal fade text-left">';
			$modals .= '<div role="document" class="modal-dialog">';
			$modals .= '<div class="modal-content">';
			$modals .= '<div class="modal-header">';
			$modals .= '<h4 id="createGroupLabel" class="modal-title">Reply Comment</h4>';
			$modals .= '<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>';
			$modals .= '</div>';
			// Content Form
			$modals .= '<form class="form-horizontal form-material" id="loginform" method="POST" action="'.$url.'">';
			$modals .= csrf_field();
			$modals .= '<div class="form-group">';
			$modals .= '<div class="col-md-12">';
			$modals .= '<label for="to">To</label>';
			$modals .= '<select name="to" class="form-control form-control-line">';
			foreach ($this->getParticipateMemo($trans_memo_id) as $key => $participate) {
				$modals .= '<option value="'.$participate.'" '.($participate == $value->from ? "selected" : "").'>';
				foreach ($employees as $key => $employee) {
					$modals .= ($employee->number == $participate ? $employee->fullname : "");
				}
				$modals .= '</option>';
			}
			$modals .= '</select>';
			$modals .= '</div>';
			$modals .= '</div>';
			$modals .= '<div class="form-group">';
			$modals .= '<div class="col-md-12">';
			$modals .= '<label for="message">Message</label>';
			$modals .= '<textarea name="message" class="form-control form-control-line"></textarea>';
			$modals .= '<input type="hidden" name="parent_id" value="'.($value->parent_id == 0 ? $value->id : $value->parent_id).'">';
			$modals .= '<input type="hidden" name="from" value="'.$this->permissions->getNIK_byEmail().'">';
			$modals .= '<input type="hidden" name="trans_memo_id" value="'.$value->trans_memo_id.'">';
			$modals .= '</div>';
			$modals .= '</div>';
			$modals .= '<div class="modal-footer">';
			$modals .= '<button type="submit" class="btn btn-success">Submit</button>';
			$modals .= '</div>';
			$modals .= '</form>';
			// End Content Form
			$modals .= '</div>';
			$modals .= '</div>';
			$modals .= '</div>';
		}

		return $modals;   
	}

	function getInputType($name, $value, $type, $dataenum = false)
	{
		$employees = $this->Masterdata->getEmployee()->get();

			$enum = explode(',',$dataenum);
			if($enum) {
				array_walk($enum, 'trim');
			}

		$s = '';
		switch ($type) {
			case 'text':
				$s .= '<input type="text" class="form-control form-control-line" name="'.$name.'" id="'.$name.'" value="'.$value.'"/>';
				break;
			case 'email':
				$s .= '<input type="email" class="form-control form-control-line" name="'.$name.'" id="'.$name.'" value="'.$value.'"/>';
				break;
			case 'textarea':
				$s .= '<textarea class="form-control form-control-line" name="'.$name.'" id="'.$name.'">'.$value.'</textarea>';
				break;
			case 'select':
				$s .= '<select class="form-control form-control-line select2" name="'.$name.'" id="'.$name.'">';
				$s .= '<option value="">-- Choose --</option>';
				if($dataenum):
					foreach($enum as $val) {
					$s .= '<option $selected value="'.$val.'" '.($val == $value ? "selected" : "").'>'.$val.'</option>';
					}
				endif;
				$s .= '</select>';
				break;
			case 'select_employee':
				$s .= '<select class="form-control form-control-line select2" name="'.$name.'" id="'.$name.'">';
				$s .= '<option value="">-- Choose --</option>';
				foreach($employees as $employee){
					$s .= '<option value="'.$employee->number.'" '.($employee->number == $value ? "selected" : "").'>'.$employee->fullname.'</option>';
				}

				$s .= '</select>';
				break;
		}

		return $s;
	}

	function getDateDiff($date_start, $date_finish)
	{
		$start = date_create($date_start);
		$finish = date_create($date_finish);
		$diff = date_diff($finish, $start);

		$datediff = $diff->d." Days ".$diff->h." Hours ".$diff->i." Minutes";

		return $datediff;
		// $datediff = date_diff(date_create("2018-05-01 17:50:00"), date_create("2018-04-01 17:40:20"));
		// 												echo $datediff->y." Year ".$datediff->m." Month ".$datediff->d." Day ".$datediff->h." Hour ".$datediff->i." Minutes ". $datediff->s." Second ";
	}
}
