<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Http\permissions;
use App\Memo;
use Auth;
use DB;

class HomeController extends Controller
{
    protected $layout = "layouts.app";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->permissions = new permissions;
        $this->Memo = new Memo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }

    public function dashboard()
    {
        $data = [
            'title' => 'Dashboard'
        ];

        $user_group = $this->permissions->get_user_groups();
        
        // Start - Get NIK
        $email_user = Auth::user()->email;
        $created_by = $this->permissions->getNIK_byEmail($email_user);
        // End

        switch ($user_group[0]->name) {
            case 'User':
                $data['sum']['total_memo'] = $this->Memo->getByCreated($created_by)->count(DB::raw('distinct trans_memo_code'));
                $data['sum']['draft'] = $this->Memo->sumDraft($created_by)->count(DB::raw('distinct trans_memo_code'));
                $data['sum']['request'] = $this->Memo->sumRequest($created_by)->count(DB::raw('distinct trans_memo_code'));
                $data['sum']['approve'] = $this->Memo->sumApprove($created_by)->count(DB::raw('distinct trans_memo_code'));
                $data['sum']['revise'] = $this->Memo->sumRevise($created_by)->count(DB::raw('distinct trans_memo_code'));
                $data['sum']['reject'] = $this->Memo->sumReject($created_by)->count(DB::raw('distinct trans_memo_code'));
                break;
            
            default:
                $data['sum']['total_memo'] = $this->Memo->getByCreated()->count();
                $data['sum']['draft'] = $this->Memo->sumDraft()->count();
                $data['sum']['request'] = $this->Memo->sumRequest()->count();
                $data['sum']['approve'] = $this->Memo->sumApprove()->count();
                $data['sum']['revise'] = $this->Memo->sumRevise()->count();
                $data['sum']['reject'] = $this->Memo->sumReject()->count();
                break;
        }

        return view('dashboard', compact('data'));
    }
}
