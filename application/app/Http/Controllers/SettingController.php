<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\permissions;
use Alert;
use Validator;
use DB;
use App\Http\helpers;
use App\sm;
use App\Masterdata;
use Redirect;
use Auth;

class SettingController extends Controller
{
    //
    public function __construct()
    {
    	$this->permissions = new permissions;
		$this->helpers = new helpers;
		$this->sm = new sm;
        $this->Masterdata = new Masterdata;
    }

    // Start Approval Type
    public function approvalType()
    {
    	if ($this->permissions->isAllowed('setting_approval-type')) {
    		$data = array(
    			'title' => 'Approval Type List',
    			'approvaltype' => $this->sm->getApprovalType()->get()
    		);

    		return view('setting.approval_type', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function approvalTypeCreate(Request $request)
    {
    	if ($this->permissions->isAllowed('master_approval-type_create')) {
    		$validator = Validator::make($request->all(), [
    			'approval_name' => 'required|max:100|unique:master_approval'
    		]);

    		if ($validator->fails()) {
            	return redirect('setting/approval-type')
                        	->withErrors($validator)
                        	->withInput();
        	} else {
        		$approval_name = $request->input('approval_type_name');
        		$data = [
        			'approval_name' => $approval_name,
        			'created_at' => date('Y-m-d H:i:s'),
        		];
        		
        		DB::table('master_approval')
        			->insert($data);
        		Alert::success("New Approval Type Created.", "Success");

        		return redirect("setting/approval-type");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function approvalTypeUpdate(Request $request, $id)
    {
    	if ($this->permissions->isAllowed('master_approval-type_update')) {
    		$validator = Validator::make($request->all(), [
    			'approval_name' => 'required|max:100'
    		]);

    		if ($validator->fails()) {
            	return redirect('setting/approval-type')
                        	->withErrors($validator);
        	} else {
        		$approval_name = $request->input('approval_name');
        		$data = [
        			'approval_name' => $approval_name,
        			'updated_at' => date('Y-m-d H:i:s'),
        		];
        		$where = ['id' => $id];
        		
        		DB::table('master_approval')
        			->where($where)
        			->update($data);
        		Alert::success("Approval Type Updated.", "Success");

        		return redirect("setting/approval-type");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function approvalTypeDelete($id)
    {
    	if ($this->permissions->isAllowed('master_approval-type_delete')) {
    		$data = [
        		'is_deleted' => 1,
        		'deleted_at' => date('Y-m-d H:i:s'),
        	];
        	$where = ['id' => $id];

        	DB::table('master_approval')
        		->where($where)
        		->update($data);
        	Alert::success("Approval Type Deleted.", "Success");

        	return redirect("setting/approval-type");
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }
    // End Approval

    // Start Memo Approver
    public function memoApprover()
    {
        if ($this->permissions->isAllowed('master_memo-approver')) {
            $data = [
                'title' => "List Memo",
                'memo' => $this->Masterdata->getMemo()->get()
            ];

            return view('setting.memo_approver', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function memoApproverView($id)
    {
        if ($this->permissions->isAllowed('master_memo-approver_view')) {
            $data = [
                'title' => "Memo Detail",
                'memo' => $this->Masterdata->getMemoById($id)->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
                'approvaltype' => $this->sm->getApprovalType()->get(),
                'approver' => $this->sm->getApproverMemo($id)->get()
            ];

            return view('setting.memo_approver_view', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function memoApproverCreate(Request $request)
    {
        if ($this->permissions->isAllowed('master_memo-approver_create')) {
            $memo_id = $request->input('memo_id');
            $validator = Validator::make($request->all(), [
                'employee' => 'required|not_in:-- Choose --',
                'approval_type' => 'required|not_in:-- Choose --',
                'order' => 'numeric',
            ]);

            if ($validator->fails()) {
                return redirect('setting/memo-approver-view/'.$memo_id)
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $data = [
                    'memo_id' => $memo_id,
                    'nik' => $request->input('employee'),
                    'approval_id' => $request->input('approval_type'),
                    'order' => $request->input('order'),
                ];
                
                DB::table('memo_approver')
                    ->insert($data);
                Alert::success("Memo Approver Added.", "Success");

                return redirect('setting/memo-approver-view/'.$memo_id);
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function memoApproverUpdate(Request $request, $id)
    {
        if ($this->permissions->isAllowed('master_memo-approver_update')) {
            $memo_id = $request->input('memo_id');
            $validator = Validator::make($request->all(), [
                'employee' => 'required|not_in:-- Choose --',
                'approval_type' => 'required|not_in:-- Choose --',
                'order' => 'numeric',
            ]);

            if ($validator->fails()) {
                return redirect('setting/memo-approver-view/'.$memo_id)
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $where = [
                    'id' => $id
                ];
                $data = [
                    'memo_id' => $memo_id,
                    'nik' => $request->input('employee'),
                    'approval_id' => $request->input('approval_type'),
                    'order' => $request->input('order'),
                ];
                
                DB::table('memo_approver')
                    ->where($where)
                    ->update($data);
                Alert::success("Memo Approver Updated.", "Success");

                return redirect('setting/memo-approver-view/'.$memo_id);
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");   
        }
    }

    public function memoApproverDelete($id)
    {
        if ($this->permissions->isAllowed('master_memo-approver_delete')) {
            $where = ['id' => $id];

            DB::table('memo_approver')
                ->where($where)
                ->delete();

            Alert::success("Memo Approver Deleted.", "Success");

            return Redirect::back();
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");   
        }
    }
    // End Memo Approver

    // Start Groups
    public function group()
    {
        if ($this->permissions->isAllowed('setting_groups')) {
            $data = array(
                'title' => 'Groups List',
                'group' => $this->sm->getGroup()->get()
            );

            return view('setting.group', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }    
    }

    public function groupCreate(Request $request)
    {
        if ($this->permissions->isAllowed('setting_groups_create')) {
            $validator = Validator::make($request->all(), [
                'group_name' => 'required|max:50',
                'description' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
            } else {
                $group_name = $request->input('group_name');
                $description = $request->input('description');
                $data = [
                    'name' => $group_name,
                    'desc' => $description,
                ];
                
                DB::table('master_group')
                    ->insert($data);
                Alert::success("Group Added.", "Success");

                return Redirect::back();
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }    
    }

    public function groupUpdate(Request $request, $id)
    {
        if ($this->permissions->isAllowed('setting_groups_update')) {
            $validator = Validator::make($request->all(), [
                'group_name' => 'required|max:50',
                'description' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                        ->withErrors($validator);
            } else {
                $group_name = $request->input('group_name');
                $description = $request->input('description');
                $data = [
                    'name' => $group_name,
                    'desc' => $description,
                ];
                $where = ['id' => $id];
                
                DB::table('master_group')
                    ->where($where)
                    ->update($data);
                Alert::success("Group Updated.", "Success");

                return Redirect::back();
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }    
    }
    // End Groups

    // Start Users
    public function users()
    {
        if ($this->permissions->isAllowed('setting_users')) {
            $data = array(
                'title' => 'Users List',
                'group' => $this->sm->getGroup()->get(),
                'users' => $this->sm->getUsers()->get(),
            );

            return view('setting.users', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function usersUpdate(Request $request, $id)
    {
        if ($this->permissions->isAllowed('setting_users_update')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:150',
                'email' => 'required|max:150',
                'group' => 'required|not_in:-- Choose --'
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                        ->withErrors($validator);
            } else {
                $name = $request->input('name');
                $email = $request->input('email');
                $group = $request->input('group');
                $old_group = $request->input('old_group');
                $data = [
                    'name' => $name,
                    'email' => $email,
                ];
                $where = ['id' => $id];
                
                DB::table('users')
                    ->where($where)
                    ->update($data);

                $whereUserGroup['group_id'] = $old_group;
                $whereUserGroup['user_id'] = $id;
                DB::table('user_group')
                    ->where($whereUserGroup)
                    ->update(['group_id' => $group]);

                Alert::success("Group Updated.", "Success");

                return Redirect::back();
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function generateNewUsers()
    {
        if ($this->permissions->isAllowed('setting_users_create')) {
            ini_set('max_execution_time', 300);
            $employees = $this->Masterdata->getEmployee()->get();

            $count = 0;
            $email = '';
            foreach ($employees as $key => $employee) {
                $where = ['email' => $employee->email];

                $getUser = $this->sm->getWhere($where, 'users')->get();
                if ($getUser->count() == 0) {
                    $email .= ($email != '') ? ',' : '';
                    $email .= $employee->email;

                    $password = '123456';
                    $now = date("Y-m-d H:i:s");
                    $data = [
                        'name' => $employee->fullname,
                        'username' => $employee->number,
                        'email' => $employee->email,
                        'password' => bcrypt($password),
                        'created_at' => $now,
                    ];

                    $insertedId = DB::table('users')->insertGetId($data);

                    // Group USER
                    $user_group = 3;
                    //

                    $data = [
                        'user_id' => $insertedId,
                        'group_id' => $user_group
                    ];

                    DB::table('user_group')->insert($data);

                    ++$count;
                }
            }

            
            Alert::success($count." Users generated. ".$email, "Success");

            return Redirect::back();
        } else {
            return Redirect::back()
                    ->withErrors("You do not have permission");   
        }
    }

    public function updateAllUsername()
    {
        $getUsers = DB::table('users')->get();

        if ($getUsers->count() > 0) {
            foreach ($getUsers as $key => $value) {
                $getNIK = $this->permissions->getNIK_byEmail($value->email);
                $getEmployee = $this->Masterdata->getEmployeeByNumber($getNIK)->first();
                $where = ['email' => $value->email];
                $data = ['username' => $getEmployee->number];

                DB::table('users')->where($where)->update($data);
            }
        }
    }
    // End Users

    // Start Application Setting
    public function appSetting()
    {
        if ($this->permissions->isAllowed('setting_app')) {
            $data = [
                'title' => 'Application Setting',
                'setting' => $this->sm->getAppSetting()->get()
            ];

            return view('setting.setting_app', compact('data'));
        } else {
            return Redirect::back()
                    ->withErrors("You do not have permission");   
        }
    }
    // End Application Setting

    // Start Password
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $data = [
                'password' => bcrypt($request->input('password')),
                'updated_at' => date("Y-m-d H:i:s"),
            ];

            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update($data);

            alert()->success("Password changed successfully.", "Nice!");

            return back();
        }
    }

    // End Password
}
