<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\permissions;
use Alert;
use App\Masterdata;
use Validator;
use DB;
use App\Http\helpers;
use App\sm;

class MasterdataController extends Controller
{
    //
    public function __construct()
    {
    	// parent::__construct();
    	$this->permissions = new permissions;
    	$this->Masterdata = new Masterdata;
    	$this->helpers = new helpers;
        $this->sm = new sm;

    }

    // Start Initiated Master
    public function initiated()
    {
        //cek apakah pengguna diizinkan masuk kedalam sistem
    	if ($this->permissions->isAllowed('master_initiated')) {
    		$data = array(
    			'title' => 'Master Initiated',
    			'initiated' => $this->Masterdata->getInitiated()->get()
    		);

    		return view('masterdata.masterdata_initiated', compact('data'));
    	}

        //jika user maka akan diarahkan ke home
        else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function initiatedCreate(Request $request)
    {
    	if ($this->permissions->isAllowed('master_initiated_create')) {
    		$validator = Validator::make($request->all(), [
    			'initiated_name' => 'required|max:100|unique:master_initiated'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/initiated')
                        	->withErrors($validator)
                        	->withInput();
        	} else {
        		$initiated_name = $request->input('initiated_name');
        		$data = [
        			'initiated_name' => $initiated_name,
        			'created_at' => date('Y-m-d H:i:s'),
        		];
        		
        		DB::table('master_initiated')
        			->insert($data);
        		Alert::success("New Initiated Created.", "Success");

        		return redirect("master/initiated");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function initiatedUpdate(Request $request, $id)
    {
    	if ($this->permissions->isAllowed('master_initiated_update')) {
    		$validator = Validator::make($request->all(), [
    			'initiated_name' => 'required|max:100'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/initiated')
                        	->withErrors($validator);
        	} else {
        		$initiated_name = $request->input('initiated_name');
        		$data = [
        			'initiated_name' => $initiated_name,
        			'updated_at' => date('Y-m-d H:i:s'),
        		];
        		$where = ['id' => $id];
        		
        		DB::table('master_initiated')
        			->where($where)
        			->update($data);
        		Alert::success("Initiated Updated.", "Success");

        		return redirect("master/initiated");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function initiatedDelete($id)
    {
    	if ($this->permissions->isAllowed('master_initiated_delete')) {
    		$data = [
        		'is_deleted' => 1,
        		'deleted_at' => date('Y-m-d H:i:s'),
        	];
        	$where = ['id' => $id];

        	DB::table('master_initiated')
        		->where($where)
        		->update($data);
        	Alert::success("Initiated Deleted.", "Success");

        	return redirect("master/initiated");
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

	public function initiatedView($id)
    {
    	if ($this->permissions->isAllowed('master_initiated_view')) {
    		$data = array(
    			'title' => 'Initiated Detail',
    			'initiated' => $this->Masterdata->getInitiatedById($id)->get(),
    			'employee' => $this->Masterdata->getEmployee()->get(),
    			'initiated_employee' => $this->Masterdata->getEmployeeByInitiatedId($id)->get(),
    		);

    		return view('masterdata.masterdata_initiated_view', compact('data'));
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function initiatedEmployeeUpdate(Request $request, $id)
    {
        if ($this->permissions->isAllowed('master_initiated-employee_update')) {
            // $validator = Validator::make($request->all(), [
            //     'initiated_name' => 'required|max:100'
            // ]);

            // if ($validator->fails()) {
            //     return redirect('master/initiated')
            //                 ->withErrors($validator);
            // } else {
                $pic = $request->input('pic');
                $where = ['initiated_id' => $id];
                // Delete All Initiated Employee by id
                DB::table('initiated_employee')
                    ->where($where)
                    ->delete();
                
                $data = [
                    'initiated_id' => $id,
                ];

                // Re-insert
                foreach ($pic as $key => $nik) {
                    $data['nik'] = $nik;
                    DB::table('initiated_employee')
                        ->insert($data);
                }

                Alert::success("Initiated Employee Updated.", "Success");

                return redirect("master/initiated-view/".$id);
            // }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");  
        }
    }
    // End Initiated Master

    // Start Memo Format
    public function memoFormat()
    {
    	if ($this->permissions->isAllowed('master_memo-format')) {
    		$data = array(
    			'title' => 'Master Format List',
    			'memoformat' => $this->Masterdata->getMemoFormat()->get()
    		);

    		return view('masterdata.masterdata_memoformat', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function memoFormatCreate(Request $request)
    {
    	if ($this->permissions->isAllowed('master_memo-format_create')) {
    		$validator = Validator::make($request->all(), [
    			'format_name' => 'required|max:100|unique:master_memoformat'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/memo-format')
                        	->withErrors($validator)
                        	->withInput();
        	} else {
        		$format_name = $request->input('format_name');
        		$data = [
        			'format_name' => $format_name,
        			'created_at' => date('Y-m-d H:i:s'),
        		];
        		
        		DB::table('master_memoformat')
        			->insert($data);
        		Alert::success("New Memo Format Created.", "Success");

        		return redirect("master/memo-format");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function memoFormatUpdate(Request $request, $id)
    {
    	if ($this->permissions->isAllowed('master_memo-format_update')) {
    		$validator = Validator::make($request->all(), [
    			'format_name' => 'required|max:100'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/memo-format')
                        	->withErrors($validator);
        	} else {
        		$format_name = $request->input('format_name');
        		$data = [
        			'format_name' => $format_name,
        			'updated_at' => date('Y-m-d H:i:s'),
        		];
        		$where = ['id' => $id];
        		
        		DB::table('master_memoformat')
        			->where($where)
        			->update($data);
        		Alert::success("Memo Format Updated.", "Success");

        		return redirect("master/memo-format");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function memoFormatDelete($id)
    {
    	if ($this->permissions->isAllowed('master_memo-format_delete')) {
    		$data = [
        		'is_deleted' => 1,
        		'deleted_at' => date('Y-m-d H:i:s'),
        	];
        	$where = ['id' => $id];

        	DB::table('master_memoformat')
        		->where($where)
        		->update($data);
        	Alert::success("Memo Format Deleted.", "Success");

        	return redirect("master/memo-format");
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }
    // End Memo Format

    // Start Memo Type
    public function memoType()
    {
    	if ($this->permissions->isAllowed('master_memo-type')) {
    		$data = array(
    			'title' => 'Master Memo Type List',
    			'memotype' => $this->Masterdata->getMemoType()->get()
    		);
    		// dd($this->Masterdata->getEmployee());
    		return view('masterdata.masterdata_memotype', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function memoTypeCreate(Request $request)
    {
    	if ($this->permissions->isAllowed('master_memo-type_create')) {
    		$validator = Validator::make($request->all(), [
    			'type_name' => 'required|max:100|unique:master_memotype'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/memo-type')
                        	->withErrors($validator)
                        	->withInput();
        	} else {
        		$type_name = $request->input('type_name');
        		$data = [
        			'type_name' => $type_name,
        			'created_at' => date('Y-m-d H:i:s'),
        		];
        		
        		DB::table('master_memotype')
        			->insert($data);
        		Alert::success("New Memo Type Created.", "Success");

        		return redirect("master/memo-type");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function memoTypeUpdate(Request $request, $id)
    {
    	if ($this->permissions->isAllowed('master_memo-type_update')) {
    		$validator = Validator::make($request->all(), [
    			'type_name' => 'required|max:100'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/memo-type')
                        	->withErrors($validator);
        	} else {
        		$type_name = $request->input('type_name');
        		$data = [
        			'type_name' => $type_name,
        			'updated_at' => date('Y-m-d H:i:s'),
        		];
        		$where = ['id' => $id];

        		DB::table('master_memotype')
        			->where($where)
        			->update($data);
        		Alert::success("Memo Type Updated.", "Success");

        		return redirect("master/memo-type");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function memoTypeDelete($id)
    {
    	if ($this->permissions->isAllowed('master_memo-type_delete')) {
    		$data = [
        		'is_deleted' => 1,
        		'deleted_at' => date('Y-m-d H:i:s'),
        	];
        	$where = ['id' => $id];

        	DB::table('master_memotype')
        		->where($where)
        		->update($data);
        	Alert::success("Memo Type Deleted.", "Success");

        	return redirect("master/memo-type");
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }
    // End Memo Format

    // Start LOA
    public function loa()
    {
    	if ($this->permissions->isAllowed('master_loa')) {
    		$data = array(
    			'title' => 'Master LOA',
    			'loa' => $this->Masterdata->getLOA()->get()
    		);
    		// dd($this->Masterdata->getEmployee());
    		return view('masterdata.masterdata_loa', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function loaCreate(Request $request)
    {
    	if ($this->permissions->isAllowed('master_loa_create')) {
    		$validator = Validator::make($request->all(), [
    			'loa_name' => 'required|max:100|unique:master_loa'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/loa')
                        	->withErrors($validator)
                        	->withInput();
        	} else {
        		$loa_name = $request->input('loa_name');
        		$data = [
        			'loa_name' => $loa_name,
        			'created_at' => date('Y-m-d H:i:s'),
        		];
        		
        		DB::table('master_loa')
        			->insert($data);
        		Alert::success("New LOA Created.", "Success");

        		return redirect("master/loa");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function loaUpdate(Request $request, $id)
    {
    	if ($this->permissions->isAllowed('master_loa_update')) {
    		$validator = Validator::make($request->all(), [
    			'loa_name' => 'required|max:100'
    		]);

    		if ($validator->fails()) {
            	return redirect('master/loa')
                        	->withErrors($validator);
        	} else {
        		$loa_name = $request->input('loa_name');
        		$data = [
        			'loa_name' => $loa_name,
        			'updated_at' => date('Y-m-d H:i:s'),
        		];
        		$where = ['id' => $id];

        		DB::table('master_loa')
        			->where($where)
        			->update($data);
        		Alert::success("LOA Updated.", "Success");

        		return redirect("master/loa");
        	}
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }

    public function loaDelete($id)
    {
    	if ($this->permissions->isAllowed('master_loa_delete')) {
    		$data = [
        		'is_deleted' => 1,
        		'deleted_at' => date('Y-m-d H:i:s'),
        	];
        	$where = ['id' => $id];

        	DB::table('master_loa')
        		->where($where)
        		->update($data);
        	Alert::success("LOA Deleted.", "Success");

        	return redirect("master/loa");
        } else {
        	return redirect('home')
                    ->withErrors("You do not have permission");	
        }
    }
    // End LOA

    // Start Memo
    public function memo()
    {
    	if ($this->permissions->isAllowed('master_memo')) {
    		$data = array(
    			'title' => 'Master Memo',
    			'memo' => $this->Masterdata->getMemo()->get()
    		);
    		
    		return view('masterdata.masterdata_memo', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function memoCreate()
    {
    	if ($this->permissions->isAllowed('master_memo_create')) {
    		$data = array(
    			'title' => 'Create New Memo',
    			'initiated' => $this->Masterdata->getInitiated()->get(),
    			'loa' => $this->Masterdata->getLOA()->get(),
    			'memotype' => $this->Masterdata->getMemoType()->get(),
    			'memoformat' => $this->Masterdata->getMemoFormat()->get(),
    			'branch' => $this->Masterdata->getBranch()->get(),
    			'boolean' => $this->Masterdata->getParam('boolean')->get(),
    		);
    		
    		return view('masterdata.master_memo.create_memo', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}	
    }

    public function memoStore(Request $request)
    {
    	if ($this->permissions->isAllowed('master_memo_create')) {
    		$validator = Validator::make($request->all(), [
    			// 'entity' => 'required|not_in:-- Choose --',
    			// 'division' => 'required|not_in:-- Choose --',
    			// 'department' => 'required|not_in:-- Choose --',
                'memo_code' => 'required|max:50|unique:master_memo',
                'memo_name' => 'required|max:255',
    			'remarks' => 'required|max:1000',
    			'memo_type' => 'required|not_in:-- Choose --',
    			'memo_format' => 'required|not_in:-- Choose --',
    			'initiated' => 'required|not_in:-- Choose --',
    			'loa' => 'required',
    			'last_approval' => 'required',
                'is_zone' => 'required',
    		]);

    		if ($validator->fails()) {
            	return redirect('master/memo-create')
                        	->withErrors($validator)
                        	->withInput();
            } else {
            	$data = [
            		// 'memo_code' => $this->helpers->getMemoCode(),
                    'memo_code' => $request->input('memo_code'),
            		// 'entity_id' => $request->input('entity'),
            		// 'division_id' => $request->input('division'),
            		// 'dept_id' => $request->input('department'),
            		'memo_name' => $request->input('memo_name'),
            		'remarks' => $request->input('remarks'),
            		'memotype_id' => $request->input('memo_type'),
            		'memoformat_id' => $request->input('memo_format'),
            		'initiated_id' => $request->input('initiated'),
            		'is_last_approval' => $request->input('last_approval'),
                    'is_zone' => $request->input('is_zone'),
            		'created_at' => date("Y-m-d H:i:s")
            	];
            	 $last_id = DB::table('master_memo')->insertGetId($data);

            	 $loa = $request->input('loa');
            	 foreach ($loa as $key => $loa_id) {
            	 	DB::table('memo_loa')
            	 		->insert([
            	 			'memo_id' => $last_id,
            	 			'loa_id' => $loa_id
            	 		]);
            	 }

                 if ($request->input('last_approval') == 1) {
                    $where = ['name' => 'last_approval'];
                    $lastApproval = $this->sm->getValueAppSetting($where);
                    
                    $data = [
                        'memo_id' => $last_id,
                        'nik' => $lastApproval,
                        'approval_id' => 99,
                        'order' => 1
                    ];
                    DB::table('memo_approver')->insert($data);
                 }
                 Alert::success("Memo Created.", "Success");

            	 return redirect('master/memo');
            }

    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function memoView($id)
    {
    	if ($this->permissions->isAllowed('master_memo_view')) {
    		$memo = $this->Masterdata->getMemoById($id)->get();
    		$data = array(
    			'title' => 'Memo Detail',
    			'memo' => $memo,
    			'initiated' => $this->Masterdata->getInitiatedById($memo[0]->initiated_id)->get(),
    			'loa' => $this->Masterdata->getLOAByMemoId($id)->get(),
    			'memotype' => $this->Masterdata->getMemoTypeById($memo[0]->memotype_id)->get(),
    			'memoformat' => $this->Masterdata->getMemoFormatById($memo[0]->memoformat_id)->get(),
    			'entity' => $this->Masterdata->getBranchById($memo[0]->entity_id)->get(),
    			'division' => $this->Masterdata->getOrganizationById($memo[0]->division_id)->get(),
    			'department' => $this->Masterdata->getOrganizationById($memo[0]->dept_id)->get(),
    			'last_approval' => $this->Masterdata->getParam('boolean')->get()[$memo[0]->is_last_approval],
                'is_zone' => $this->Masterdata->getParam('boolean')->get()[$memo[0]->is_zone],
    		);
    		
    		return view('masterdata.master_memo.view_memo', compact('data'));
    	} else {
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}	
    }

    public function memoEdit($id)
    {
        if ($this->permissions->isAllowed('master_memo_update')) {
            $memo = $this->Masterdata->getMemoById($id)->get();
            $data = array(
                'title' => 'Form Update Memo',
                'memo' => $memo,
                'initiated' => $this->Masterdata->getInitiated()->get(),
                'loa' => $this->Masterdata->getLOA()->get(),
                'memoloa' => $this->Masterdata->getLOAByMemoId($id)->get(),
                'memotype' => $this->Masterdata->getMemoType()->get(),
                'memoformat' => $this->Masterdata->getMemoFormat()->get(),
                'branch' => $this->Masterdata->getBranch()->get(),
                'organization' => $this->Masterdata->getOrganization()->get(),
                'boolean' => $this->Masterdata->getParam('boolean')->get(),
            );
            
            return view('masterdata.master_memo.edit_memo', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function memoUpdate(Request $request, $id)
    {
        if ($this->permissions->isAllowed('master_memo_update')) {

            $rules = [
                // 'entity' => 'required|not_in:-- Choose --',
                'memo_name' => 'required|max:255',
                'remarks' => 'required|max:1000',
                'memo_type' => 'required|not_in:-- Choose --',
                'memo_format' => 'required|not_in:-- Choose --',
                'initiated' => 'required|not_in:-- Choose --',
                'loa' => 'required|not_in: ',
                'last_approval' => 'required',
                'is_zone' => 'required',
            ];
            
            if (array_key_exists("division", $request->all())) {
                $rules['division'] = "required|not_in:-- Choose --";
                $rules['department'] = "required|not_in:-- Choose --";
                // dd($rules);
            }

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect('master/memo-edit/'.$id)
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $data = [
                    // 'entity_id' => $request->input('entity'),
                    'memo_name' => $request->input('memo_name'),
                    'remarks' => $request->input('remarks'),
                    'memotype_id' => $request->input('memo_type'),
                    'memoformat_id' => $request->input('memo_format'),
                    'initiated_id' => $request->input('initiated'),
                    'is_last_approval' => $request->input('last_approval'),
                    'is_zone' => $request->input('is_zone'),
                    'updated_at' => date("Y-m-d H:i:s")
                ];

                if (array_key_exists("division", $rules)) {
                    $data['division_id'] = $request->input('division');
                    $data['dept_id'] = $request->input('department');
                }

                $where = ['id' => $id];

                // Update Memo
                DB::table('master_memo')
                    ->where($where)
                    ->update($data);
                

                // Delete LOA
                DB::table('memo_loa')
                        ->where('memo_id', $id)
                        ->delete();

                // Re-insert LOA for Current Memo
                $loa = $request->input('loa');
                foreach ($loa as $key => $loa_id) {
                    DB::table('memo_loa')
                        ->insert([
                            'memo_id' => $id,
                            'loa_id' => $loa_id
                        ]);
                 }

                 Alert::success("Memo Updated.", "Success");

                 return redirect('master/memo');
            }

        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function memoDelete($id)
    {
        if ($this->permissions->isAllowed('master_memo_delete')) {
            $data = [
                'is_deleted' => 1,
                'deleted_at' => date('Y-m-d H:i:s'),
            ];
            $where = ['id' => $id];

            DB::table('master_memo')
                ->where($where)
                ->update($data);
            Alert::success("Memo Deleted.", "Success");

            return redirect("master/memo");
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");  
        }
    }
    // End Memo

    // Start Attr Employee
    public function getDivisionByBranch($id)
    {
    	$data = $this->Masterdata->getDivisionByBranch($id)->get();

    	return $data;
    }

    public function getDepartmentByDivision($id)
    {
    	$data = $this->Masterdata->getDepartmentByDivision($id)->get();

    	return $data;
    }
    // End Attr Employee
}
