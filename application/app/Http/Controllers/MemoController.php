<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\permissions;
use Auth;
use App\Memo;
use App\Masterdata;
use Validator;
use Redirect;
use App\Http\helpers;
use File;
use Alert;
use DB;
use App\sm;
use PDF;
use Mail;
use Crypt;

class MemoController extends Controller
{
    //
    public function __construct()
    {
    	$this->permissions = new permissions;
        $this->helpers = new helpers;
    	$this->Memo = new Memo;
        $this->Masterdata = new Masterdata;
        $this->sm = new sm;
        $this->label = [
                0 => 'label label-info', 'label label-primary', 'label label-success', 'label label-warning', 'label label-danger'
        ];
    }

    public function index()
    {
    	if ($this->permissions->isAllowed('trans_memo')) {
	    	$email_user = Auth::user()->email;
            $created_by = $this->permissions->getNIK_byEmail($email_user);

            $user_group = $this->permissions->get_user_groups();
	    	
	    	$data = [
	    		'title' => 'Memo Transaction',
	    		// 'memo' => $this->Memo->getByCreated($created_by)->distinct()->get(),
                'allmemo' => $this->Masterdata->getMemo()->get(),
                'memochar' => $this->Masterdata->getChar()->get(),
                'param' => $this->Masterdata->getParam('memo status')->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
	    	];
            $data['label'] = $this->label;

            switch ($user_group[0]->name) {
                case 'User':
                    $data['memo'] = $this->Memo->getByCreated($created_by)->distinct()->get();
                    break;
                
                default:
                    $data['memo'] = $this->Memo->getByCreated()->distinct()->get();
                    break;
            }

	    	return view('components.memo.trans_memo', compact('data'));
    	} else {
    		
    		return redirect('home')
                    ->withErrors("You do not have permission");
    	}
    }

    public function create()
    {
        if ($this->permissions->isAllowed('trans_memo_create')) {
            $email_user = Auth::user()->email;
            $user_nik = $this->permissions->getNIK_byEmail($email_user);
            $getInitiated = DB::table('initiated_employee')
                                ->where('nik', $user_nik)
                                ->select('initiated_id')
                                ->get()->pluck('initiated_id');

            $data = [
                'title' => 'Create New Memo Transaction',
                'memo' => $this->Masterdata->getMemoByInitiated($getInitiated)->get(),
                'memochar' => $this->Masterdata->getChar()->get()
            ];

            return view('components.memo.trans_memo_create', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function store(Request $request)
    {
        if ($this->permissions->isAllowed('trans_memo_create')) {
            $email_user = Auth::user()->email;
            $created_by = $this->permissions->getNIK_byEmail($email_user);

            $validator = Validator::make($request->all(), [
                'memo' => 'not_in:-- Choose --',
                'memo_char' => 'not_in:-- Choose --',
                'background' => 'required',
                'background_attachment.*' => 'required',
                'consideration' => 'required',
                'consideration_attachment.*' => 'required',
                'points_to_be_app' => 'required',
                'points_to_be_app_attachment.*' => 'required',
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
            } else {
                $fieldAttachment = [
                    'background', 'consideration', 'points_to_be_app'
                ];

                $memo_trans_id = $this->helpers->getMemoTransId();
                $path_specific = base_path()."/../assets/uploads/trans_memo/".$memo_trans_id;
                $path_web = url('/')."/assets/uploads/trans_memo/".$memo_trans_id;

                $files = $request->file();
                if (!File::exists($path_specific)) {
                    File::makeDirectory($path_specific, $mode = 0777, true, true);
                }

                $data = [];
                foreach ($fieldAttachment as $key => $field) {
                    $filename = $field."-".$memo_trans_id;
                    $files[$field."_attachment"];
                    $filepath[$field."_attachment"] = "";
                    foreach ($files[$field."_attachment"] as $i => $file) {
                        $filename = $field."_".$memo_trans_id."-".++$i.".".$file->getClientOriginalExtension(); 
                        $path = $path_web."/".$filename;
                        $file->move($path_specific, $filename);
                        $filepath[$field."_attachment"] .= ($filepath[$field."_attachment"] != '') ? ',' : '';
                        $filepath[$field."_attachment"] .= $path;
                        
                    }
                    $data[$field."_attachment"] = $filepath[$field."_attachment"];
                }

                    $memo_id = $request->input('memo');        
                    $data['trans_memo_code'] = $memo_trans_id;
                    $data['memo_id'] = $memo_id;
                    $data['char_id'] = $request->input('memo_char');
                    $data['background'] = $request->input('background');
                    $data['consideration'] = $request->input('consideration');
                    $data['points_to_be_app'] = $request->input('points_to_be_app');
                    $data['created_by'] = $created_by;
                    $data['created_at'] = date("Y-m-d H:i:s");
                
                $insertedId = DB::table('trans_memo')->insertGetId($data);

                $getMemo = DB::table('master_memo')
                            ->where('id', '=', $memo_id)
                            ->first();
                if ($getMemo->is_zone == 1) {
                    $this->Memo->insertApprovalZone($insertedId, $created_by);
                }

                $this->Memo->insertApproval($insertedId, $data['memo_id']);
                Alert::success("New Transaction Memo Created.", "Success");

                return redirect("trans/memo");
            }
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function show($id)
    {
        if ($this->permissions->isAllowed('trans_memo_view')) {
            $data = [
                'title' => 'Transaction Memo Detail',
                'transmemo' => $this->Memo->getTransMemo($id)->first(),
                'allmemo' => $this->Masterdata->getMemo()->get(),
                'memochar' => $this->Masterdata->getChar()->get(),
                'param' => $this->Masterdata->getParam('memo status')->get(),
                'memoapproval' => $this->Memo->getTransMemoApproval($id)->get(),
                'approval' => $this->sm->getApprovalType()->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
                'memocomment' => $this->Memo->getTransMemoComment($id)->get(),
            ];

            $data['label'] = $this->label;

            $data['participate'] = $this->helpers->getParticipateMemo($id);

            return view('components.memo.trans_memo_view', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");   
        }
    }

    public function submitApproval(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $trans_memo_id = $request->input('id');
            $now = date("Y-m-d H:i:s");
            $status = $request->input('status');
            $where = [
                'id' => $trans_memo_id,
            ];

            $data = [
                'comment' => $request->input('comment'),
                'status' => 1,
                'initiated_date' => $now,
                'updated_at' => $now
            ];

            // IF Revised tidak akan update date initiated
            if ($status == 3) {
                unset($data['initiated_date']);
            }
            // 

            $updated = $this->Memo->updateData($where, $data, 'trans_memo');

            if ($updated) {
                // Re-submit Memo
                if ($status == 3) {
                    $data = [
                        'status' => 1,
                    ];

                    $where = [
                        'trans_memo_id' => $trans_memo_id,
                        'status' => $status,
                    ];

                    $this->Memo->updateData($where, $data, 'trans_memo_approval');

                // First Submit Memo
                } else {
                    $this->requestApprover($trans_memo_id);


                }
            }

            Alert::success("Transaction Memo Submited.", "Success");
            return Redirect::back();
        }
    }

    public function approverSubmit(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $trans_memo_id = $request->input('id');
            $status = $request->input('status');
            $approver = $request->input('approver');
            $now = date("Y-m-d H:i:s");
            $where = [
                'trans_memo_id' => $trans_memo_id,
                'approver' => $approver,
                'status' => 1
            ];

            $data = [
                'comment' => $request->input('comment'),
                'status' => $status,
                'dateInitiated' => $now,
                'updated_at' => $now
            ];

            $updated = $this->Memo->updateData($where, $data, 'trans_memo_approval');

            if ($updated) {
                $transmemo = $this->Memo->getTransMemo($trans_memo_id)->first();
                $memo = $this->Masterdata->getMemoById($transmemo->memo_id)->first();
                $initiator = $this->Masterdata->getEmployeeByNumber($transmemo->created_by)->first();
                $approverData = $this->Masterdata->getEmployeeByNumber($approver)->first();
                $attrMail['to'] = $initiator->email;
                $attrMail['subject'] = "[".$transmemo->trans_memo_code."] ".$memo->memo_code." - ".$memo->memo_name;
                $attrMail['link'] = url('/')."/trans/memo-view/".$trans_memo_id;

                // Start - Approver Above
                $approver = $this->getApproverAbove($trans_memo_id);
                if (!empty($approver)) {
                    foreach ($approver as $key => $value) {
                        $email = [
                            0 => 'amarfadhillah@gmail.com', 'amarfd.apple@gmail.com', 'setyo.wibowo@centratamagroup.com', 'fiki.azkiya@centratamagroup.com', 'dian.haryanto@centratamagroup.com'
                        ];
                        $attrMail['cc'][$key] = $value['email'];
                        // $attrMail['cc'][$key] = $email[$key];
                    }
                }
                // End

                // IF approved
                if ($status == 2) {
                $this->requestApprover($trans_memo_id);
                $attrMail['message'] = '<h3>Memo has been approved by '.$approverData->fullname."</h3>";
                } else {
                    $data = [
                        'status' => $status,
                        'updated_at' => $now,
                    ];

                    $where = [
                        'id' => $trans_memo_id,
                    ];

                    // IF Revised
                    if ($status == 3) {

                        $this->Memo->updateData($where, $data, 'trans_memo');
                        $attrMail['message'] = '<h3>Memo need to be revised from '.$approverData->fullname."</h3>";
                    // IF Rejected
                    } else {
                        $this->Memo->updateData($where, $data, 'trans_memo');

                        $data = [
                            'status' => $status,
                            'updated_at' => $now,
                        ];

                        $where = [
                            'trans_memo_id' => $trans_memo_id,
                            // all waiting status
                            'status' => 0,
                        ];

                        $this->Memo->updateData($where, $data, 'trans_memo_approval');

                        $attrMail['message'] = '<h3>Memo has been rejected by '.$approverData->fullname."</h3>";
                    }
                }
                $this->sendmail($attrMail, 'trans_memo');
            }
            Alert::success("Transaction Memo Updated.", "Success");
            return Redirect::back();
        }
    }

    public function requestApprover($trans_memo_id)
    {
        $getOneApproval = $this->Memo->getOneTransMemoApproval($trans_memo_id);
        $now = date("Y-m-d H:i:s");
        // IF still have approver
        if ($getOneApproval->count() > 0) {
            $where = [
                'trans_memo_id' => $trans_memo_id,
                'approver' => $getOneApproval[0]->approver,
            ];

            $data = [
                'status' => 1,
                'updated_at' => $now
            ];

            $transmemo = $this->Memo->getTransMemo($trans_memo_id)->first();
            $memo = $this->Masterdata->getMemoById($transmemo->memo_id)->first();
            $approver = $this->Masterdata->getEmployeeByNumber($getOneApproval[0]->approver)->first();
            $initiator = $this->Masterdata->getEmployeeByNumber($transmemo->created_by)->first();
            $attrMail['link'] = url('/')."/trans/memo-view/".$trans_memo_id;
            $attrMail['subject'] = "[".$transmemo->trans_memo_code."] Approval Memo";
            $attrMail['to'] = $approver->email;
            // $attrMail['to'] = "amarfadhillah@gmail.com";
            $attrMail['cc'] = $initiator->email;
            $attrMail['message'] = "<h4>Need Approval</h4>";
            $attrMail['message'] .= "<ul><li>Memo code(index): ".$memo->memo_code."</li><li>Memo name: ".$memo->memo_name."</li></ul>";

            $this->sendmail($attrMail, 'trans_memo');
            $this->Memo->updateData($where, $data, 'trans_memo_approval');
        } else {
            $count = $this->Memo->getTransMemoApproval($trans_memo_id)->count();
            $sumStatus = $this->Memo->getTransMemoApproval($trans_memo_id)->sum('status');

            if ($sumStatus == $count*2) {
                $where = [
                    'id' => $trans_memo_id,
                ];

                $data = [
                    'status' => 2,
                    'updated_at' => $now,
                ];

                $this->Memo->updateData($where, $data, 'trans_memo');
            }
        }
    }

    public function submitComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'comment' => 'required',
                'to' => 'not_in:-- Choose --'
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $sender = Auth::user()->email;
            $sender_nik = $this->permissions->getNIK_byEmail($sender);
            $to = $request->input('to');
            $comment = $request->input('comment');
            $trans_memo_id = $request->input('id');
            $now = date("Y-m-d H:i:s");

            $data = [
                'to' => $to,
                'trans_memo_id' => $trans_memo_id,
                'from' => $sender_nik,
                'message' => $comment,
                'created_at' => $now
            ];
            
            DB::table('trans_memo_comment')->insert($data);

            // Start Email
            $participate = $this->helpers->removeArrayItem($this->helpers->getParticipateMemo($trans_memo_id), $to);
            $transmemo = $this->Memo->getTransMemo($trans_memo_id)->first();
            $memo = $this->Masterdata->getMemoById($transmemo->memo_id)->first();
            $getSender = $this->Masterdata->getEmployeeByNumber($sender_nik)->first();
            $getTo = $this->Masterdata->getEmployeeByNumber($to)->first();
            $attrMail['subject'] = "[".$transmemo->trans_memo_code."] ".$getSender->fullname." mentioned ".$getTo->fullname." in a comment";
            $attrMail['link'] = url('/')."/trans/memo-view/".$trans_memo_id;
            $attrMail['to'] = $getTo->email;
            foreach ($participate as $key => $value) {
                $getOther = $this->Masterdata->getEmployeeByNumber($value)->first();
                $attrMail['cc'][$key] = $getOther->email;
            }
            $attrMail['message'] = "<h4>".$getSender->fullname.":</h4>";
            $attrMail['message'] .= "<blockquote>".$comment."</blockquote>";
            $this->sendmail($attrMail, 'trans_memo');
            // End Email

            Alert::success("Comment Added.", "Success");
            return Redirect::back();
        }
    }

    public function replyComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'message' => 'required',
                'to' => 'not_in:-- Choose --'
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $to = $request->input('to');
            $from = $request->input('from');
            $parent_id = $request->input('parent_id');
            $message = $request->input('message');
            $trans_memo_id = $request->input('trans_memo_id');
            $now = date("Y-m-d H:i:s");

            $data = [
                'to' => $to,
                'trans_memo_id' => $trans_memo_id,
                'from' => $from,
                'message' => $message,
                'parent_id' => $parent_id,
                'created_at' => $now
            ];

            DB::table('trans_memo_comment')->insert($data);

            // Start Email
            $participate = $this->helpers->removeArrayItem($this->helpers->getParticipateMemo($trans_memo_id), $to);
            $transmemo = $this->Memo->getTransMemo($trans_memo_id)->first();
            $memo = $this->Masterdata->getMemoById($transmemo->memo_id)->first();
            $getSender = $this->Masterdata->getEmployeeByNumber($from)->first();
            $getTo = $this->Masterdata->getEmployeeByNumber($to)->first();
            $attrMail['subject'] = "[".$transmemo->trans_memo_code."] ".$getSender->fullname." mentioned ".$getTo->fullname." in a comment";
            $attrMail['link'] = url('/')."/trans/memo-view/".$trans_memo_id;
            $attrMail['to'] = $getTo->email;
            foreach ($participate as $key => $value) {
                $getOther = $this->Masterdata->getEmployeeByNumber($value)->first();
                $attrMail['cc'][$key] = $getOther->email;
            }
            $attrMail['message'] = "<h4>".$getSender->fullname.":</h4>";
            $attrMail['message'] .= "<blockquote>".$message."</blockquote>";
            $this->sendmail($attrMail, 'trans_memo');
            // End Email

            Alert::success("Replied.", "Success");
            return Redirect::back();
        }   
    }

    public function printMemo($id)
    {
        if ($this->permissions->isAllowed('trans_memo_print')) {
            $transmemo = $this->Memo->getTransMemo($id)->first();
            $data = [
                'transmemo' => $transmemo,
                'memo' => $this->Masterdata->getMemoById($transmemo->memo_id)->first(),
                'memochar' => $this->Masterdata->getChar()->get(),
                'param' => $this->Masterdata->getParam('memo status')->get(),
                'memoapproval' => $this->Memo->getTransMemoApproval($id)->get(),
                'approval' => $this->sm->getApprovalType()->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
                'memocomment' => $this->Memo->getTransMemoComment($id)->get(),
                'created' => $this->Masterdata->getEmployeeByNumber($transmemo->created_by)->first(),
                'approvaltype' => $this->sm->getApprovalType()->get(),
                'memostatus' => $this->Masterdata->getParam('memo status')->get(),
                'memoloa' => $this->Masterdata->getLOAByMemoId($transmemo->memo_id)->get(),
            ];

            $data['participate'] = $this->helpers->getParticipateMemo($id);

            $pdf = PDF::loadHTML(view('pdf.trans_memo', compact('data')))
                    ->setPaper('a4', 'landscape');

            return $pdf->stream($transmemo->trans_memo_code.'.pdf');
            // return view('pdf.trans_memo');
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }

    public function edit($id)
    {
        if ($this->permissions->isAllowed('trans_memo_update')) {
            $data = [
                'title' => 'Edit Transaction Memo',
                'transmemo' => $this->Memo->getTransMemo($id)->first(),
                'allmemo' => $this->Masterdata->getMemo()->get(),
                'memochar' => $this->Masterdata->getChar()->get(),
                'param' => $this->Masterdata->getParam('memo status')->get(),
                'memoapproval' => $this->Memo->getTransMemoApproval($id)->get(),
                'approval' => $this->sm->getApprovalType()->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
                'memocomment' => $this->Memo->getTransMemoComment($id)->get(),
            ];

            $data['label'] = $this->label;

            $data['participate'] = $this->helpers->getParticipateMemo($id);

            return view('components.memo.trans_memo_edit', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");   
        }
    }

    public function update(Request $request, $id)
    {
        if ($this->permissions->isAllowed('trans_memo_update')) {
            
            $validator = Validator::make($request->all(), [
                'memo_char' => 'not_in:-- Choose --',
                'background' => 'required',
                // 'background_attachment.*' => 'required',
                'consideration' => 'required',
                // 'consideration_attachment.*' => 'required',
                'points_to_be_app' => 'required',
                // 'points_to_be_app_attachment.*' => 'required',
            ]);

            if ($validator->fails()) {
                return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
            } else {
                $transmemo = $this->Memo->getTransMemo($id)->first();

                $memo_char = $request->input('memo_char');
                $background = $request->input('background');
                $consideration = $request->input('consideration');
                $points_to_be_app = $request->input('points_to_be_app');
                $trans_memo_code = $request->input('trans_memo_code');

                $data = [
                    'char_id' => $memo_char,
                    'background' => $background,
                    'consideration' => $consideration,
                    'points_to_be_app' => $points_to_be_app,
                    'updated_at' => date("Y-m-d H:i:s"),
                ];

                // Define field with attachment
                $fieldAttachment = [
                    'background', 'consideration', 'points_to_be_app'
                ];

                $files = $request->file();
                $path_specific = base_path()."/../assets/uploads/trans_memo/".$trans_memo_code;
                $path_web = url('/')."/assets/uploads/trans_memo/".$trans_memo_code;

                foreach ($fieldAttachment as $key => $field) {
                    if ($request->hasFile($field.'_attachment')) {
                        $files[$field."_attachment"];
                        $filepath[$field."_attachment"] = "";
                        
                        // Delete File Exist
                        $transmemo = $this->Memo->getTransMemo($id)->first();
                        $fieldAttach = $field."_attachment";
                        $fileExist = explode(",", $transmemo->$fieldAttach);
                        foreach ($fileExist as $key => $value) {
                            File::delete(str_replace($path_web, $path_specific, $value));
                        }
                        // End

                        foreach ($files[$field."_attachment"] as $i => $file) {
                            $filename = $field."_".$trans_memo_code."-".++$i.".".$file->getClientOriginalExtension(); 
                            $path = $path_web."/".$filename;
                            $file->move($path_specific, $filename);
                            $filepath[$field."_attachment"] .= ($filepath[$field."_attachment"] != '') ? ',' : '';
                            $filepath[$field."_attachment"] .= $path;
                        }
                        $data[$field."_attachment"] = $filepath[$field."_attachment"];
                    }
                }

                $where = ['id' => $id];

                DB::table('trans_memo')
                    ->where($where)
                    ->update($data);
                Alert::success("Transaction Memo Updated.", "Success");

                return redirect("trans/memo-view/".$id);
            }

        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");   
        }
    }

    private function sendmail($data, $layout)
    {
        Mail::send('emails.'.$layout, ['data' => $data],
            function($message) use ($data){
            $message->to($data['to']);
            $message->subject($data['subject']);
            if (array_key_exists('cc', $data)) {
                $message->cc($data['cc']);
            }
        });
    }

    // Mengambil data approver sebelum approver yang aktif saat ini
    public function getApproverAbove($trans_memo_id)
    {
        $approver = [];

        $getApprover = $this->Memo->getTransMemoApprovalAbove($trans_memo_id);

        if ($getApprover->count() > 0) {
            foreach ($getApprover->get() as $key => $value) {
                $employee = $this->Masterdata->getEmployeeByNumber($value->approver)->first();
                $approver[$key]['nik'] = $value->approver;
                $approver[$key]['name'] = $employee->fullname;
                $approver[$key]['email'] = $employee->email;
            }
        }

        return $approver;
    }

    public function memoApprovalRequest()
    {
        if ($this->permissions->isAllowed('trans_memo-approval-request')) {
            $email_user = Auth::user()->email;
            $approver = $this->permissions->getNIK_byEmail($email_user);
            
            $data = [
                'title' => 'Memo Approval Request',
                'memo' => $this->Memo->getByApprover($approver)->distinct()->get(),
                'allmemo' => $this->Masterdata->getMemo()->get(),
                'memochar' => $this->Masterdata->getChar()->get(),
                'param' => $this->Masterdata->getParam('memo status')->get(),
                'employee' => $this->Masterdata->getEmployee()->get(),
            ];
            $data['label'] = $this->label;

            return view('components.memo.trans_memo-approval-request', compact('data'));
        } else {
            return redirect('home')
                    ->withErrors("You do not have permission");
        }
    }
}
