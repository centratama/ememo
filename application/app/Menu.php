<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_menu';

    public function parent() {

        return $this->hasOne('App\Menu', 'menu_id', 'parent_menu');

    }

    public function children() {

        return $this->hasMany('App\Menu', 'parent_menu', 'menu_id');

    }  

    public static function tree() {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_menu', '=', NULL)->get();

    }
}
