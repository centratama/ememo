<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class sm extends Model
{
    // Start Approval
	public function getApprovalType()
	{
		$r = DB::table('master_approval')
				->where('is_deleted', 0);

		return $r;
	}
    // End

    // Start Memo Approver
	public function getApproverMemo($memo_id)
	{
		$r = DB::table('memo_approver')
				->where('memo_id', $memo_id)
				->orderBy('approval_id');

		return $r;
	}
    // End

    // Start Group
	public function getGroup()
	{
		$r = DB::table('master_group');

		return $r;
	}
    // End

    // Start Users
	public function getUsers()
	{
		$r = DB::table('users');

		return $r;
	}
    // End

	// Start App Setting
	public function getAppSetting()
	{
		$r = DB::table('settings');

		return $r;	
	}

	public function getValueAppSetting($where)
	{
		$r = DB::table('settings')
				->where($where)
				->first();

		return $r->value;;	
	}	
	// End

    public function getWhere($where, $table)
    {
    	$r = DB::table($table)
    			->where($where);

		return $r;
    }
}
