<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Masterdata extends Model
{
    // Start Memo Format
    public function getMemoFormat()
    {
    	$r = DB::table('master_memoformat')
    			->where('is_deleted', 0);

    	return $r;
    }

    public function getMemoFormatById($id)
    {
    	$r = DB::table('master_memoformat')
    			->where('is_deleted', 0)
    			->where('id', $id);

    	return $r;
    }
    // End Memo Format

    // Start Initiated
    public function getInitiated()
    {
    	$r = DB::table('master_initiated')
    			->where('is_deleted', 0);

    	return $r;
    }

    public function getInitiatedById($id)
    {
    	$r = DB::table('master_initiated')
    			->where('is_deleted', 0)
    			->where('id', $id);

    	return $r;
    }

	public function getEmployeeByInitiatedId($initiated_id)
    {
    	$r = DB::table('initiated_employee')
    			->where('initiated_id', $initiated_id);

    	return $r;
    }    

    // End Initiated

    // Start Memo Type
    public function getMemoType()
    {
    	$r = DB::table('master_memotype')
    			->where('is_deleted', 0);

    	return $r;
    }

    public function getMemoTypeById($id)
    {
    	$r = DB::table('master_memotype')
    			->where('is_deleted', 0)
    			->where('id', $id);

    	return $r;
    }

    // End Memo Type

    // Start LOA
    public function getLOA()
    {
    	$r = DB::table('master_loa')
    			->where('is_deleted', 0);

    	return $r;
    }

    public function getLOAByMemoId($memo_id)
    {
    	$r = DB::table('master_loa')
    			->join('memo_loa', 'master_loa.id', '=', 'memo_loa.loa_id')
    			->where('memo_loa.memo_id', $memo_id);

    	return $r;
    }

    // End LOA

    // Start Master Employee
    public function getEmployee()
    {
    	$now = date("Y-m-d H:i:s");
    	
    	$r = DB::connection('mysql2')
    			->table('employee as a')
    			->join('organization as b', 'a.id_organization', '=', 'b.id')
    			->leftJoin('organization as c', 'c.id', '=', 'b.parentid')
    			->leftJoin('organization as d', 'd.id', '=', 'c.parentid')
                ->join('job as e', 'a.id_job', '=', 'e.id')
    			->whereNull('resigndate')
    			->orWhere('resigndate', '>=', $now)
    			->orderBy('a.fullname')
    			->select('a.number', 'a.fullname', 'a.email', 'b.id as idlevel1', 'b.name as namelevel1'
    				,'c.id as idlevel2', 'c.name as namelevel2', 'd.id as idlevel3', 'd.name as namelevel3', 'e.id as idjob', 'e.description as job'
    			)
    			// ->get();
    	// $data = [];
    	// foreach ($r as $key => $value) {
    	// 	$data[$key] = $this->getDept($value);	
    	// }
    	;
        // dd($r->get());
    	return $r;
    }

    public function getEmployeeByNumber($number)
    {
        $now = date("Y-m-d H:i:s");
        
        $r = DB::connection('mysql2')
                ->table('employee as a')
                ->join('organization as b', 'a.id_organization', '=', 'b.id')
                ->leftJoin('organization as c', 'c.id', '=', 'b.parentid')
                ->leftJoin('organization as d', 'd.id', '=', 'c.parentid')
                ->join('job as e', 'a.id_job', '=', 'e.id')
                ->where('a.number', '=', $number)
                ->orderBy('a.fullname')
                ->select('a.number', 'a.fullname', 'a.email', 'b.id as idlevel1', 'b.name as namelevel1'
                    ,'c.id as idlevel2', 'c.name as namelevel2', 'd.id as idlevel3', 'd.name as namelevel3', 'e.id as idjob', 'e.description as job'
                )
                // ->get();
        // $data = [];
        // foreach ($r as $key => $value) {
        //  $data[$key] = $this->getDept($value);   
        // }
        ;
        // dd($r->get());
        return $r;
    }

    public function getDept($employee)
    {
    	dd($employee);
    }

    public function getBranch()
    {
    	$r = DB::connection('mysql2')
    			->table('branch')
    			->whereNotNull('parentid');

    	return $r;
    }

    public function getBranchById($branch_id)
    {
    	$r = DB::connection('mysql2')
    			->table('branch')
    			->whereNotNull('parentid')
    			->where('id', $branch_id);

    	return $r;
    }

    public function getDivisionByBranch($branch_id)
    {
    	$r = DB::connection('mysql2')
    			->table('organization')
    			->where('id_branch', $branch_id)
    			->whereNull('parentid');

    	return $r;
    }

    public function getDepartmentByDivision($division_id)
    {
    	$r = DB::connection('mysql2')
    			->table('organization')
    			->where('parentid', $division_id);

    	return $r;	
    }

    public function getOrganizationById($id)
    {
    	$r = DB::connection('mysql2')
    			->table('organization')
    			->where('id', $id);

    	return $r;	
    }

    public function getOrganization()
    {
        $r = DB::connection('mysql2')
                ->table('organization');

        return $r;  
    }
    // End Master Employee

    // Start Master Parameter
    public function getParam($param_group)
    {
    	$r = DB::table('master_parameter')
    			->where('param_grp', $param_group);

    	return $r;
    }
    // End Master Parameter

    // Start Master Memo
    public function getMemo()
    {
    	$r = DB::table('master_memo')
    			->where('is_deleted', 0);

    	return $r;	
    }

    public function getMemoById($memo_id)
    {
    	$r = DB::table('master_memo')
    			->where('is_deleted', 0)
    			->where('id', $memo_id);

    	return $r;	
    }

    public function getMemoByInitiated($initiated = [])
    {
        $r = DB::table('master_memo')
                ->where('is_deleted', 0)
                ->whereIn('initiated_id', $initiated);

        return $r;  
    }
    // End Master Memo

    // Start Memo Char
    public function getChar()
    {
        $r = DB::table('master_char')
                ->where('is_deleted', 0);

        return $r;  
    }
    // End Memo Char
}
