<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//after login form ]
Route::get('/home', function(){
	return redirect('dashboard');
});

Route::prefix('/master')->group(function () {
	Route::get('/initiated', 'MasterdataController@initiated')->middleware('auth');
	Route::post('/initiated-create', 'MasterdataController@initiatedCreate')->middleware('auth');
	Route::patch('/initiated-update/{id}', array(
	'as' => 'update-initiated',
	'uses' => 'MasterdataController@initiatedUpdate'))->middleware('auth');
	Route::patch('/initiated-update/{id}', array(
	'as' => 'update-initiated',
	'uses' => 'MasterdataController@initiatedUpdate'))->middleware('auth');
	Route::get('/initiated-delete/{id}', array(
	'as' => 'delete-initiated',
	'uses' => 'MasterdataController@initiatedDelete'))->middleware('auth');
});

//menu initiated
//Route::get('master/initiated', 'MasterdataController@initiated')->middleware('auth');
//Route::post('master/initiated-create', 'MasterdataController@initiatedCreate')->middleware('auth');
/*Route::patch('master/initiated-update/{id}', array(
	'as' => 'update-initiated',
	'uses' => 'MasterdataController@initiatedUpdate'))->middleware('auth');
Route::patch('master/initiated-update/{id}', array(
	'as' => 'update-initiated',
	'uses' => 'MasterdataController@initiatedUpdate')
)->middleware('auth');
Route::get('master/initiated-delete/{id}', array(
	'as' => 'delete-initiated',
	'uses' => 'MasterdataController@initiatedDelete')
)->middleware('auth');*/
Route::get('master/initiated-view/{id}', array(
	'as' => 'view-initiated',
	'uses' => 'MasterdataController@initiatedView')
)->middleware('auth');
Route::patch('master/initiated-employee-update/{id}', array(
	'as' => 'update-initiated-employee',
	'uses' => 'MasterdataController@initiatedEmployeeUpdate')
)->middleware('auth');

Route::get('master/memo-format', 'MasterdataController@memoFormat')->name('memo-format')->middleware('auth');
Route::post('master/memo-format-create', 'MasterdataController@memoFormatCreate')->middleware('auth');
Route::patch('master/memo-format-update/{id}', array(
	'as' => 'update-memoformat',
	'uses' => 'MasterdataController@memoFormatUpdate')
)->middleware('auth');
Route::get('master/memo-format-delete/{id}', array(
	'as' => 'delete-memoformat',
	'uses' => 'MasterdataController@memoFormatDelete')
)->middleware('auth');

Route::get('master/memo-type', 'MasterdataController@memoType')->name('memo-type')->middleware('auth');
Route::post('master/memo-type-create', 'MasterdataController@memoTypeCreate')->middleware('auth');
Route::patch('master/memo-type-update/{id}', array(
	'as' => 'update-memotype',
	'uses' => 'MasterdataController@memoTypeUpdate')
)->middleware('auth');
Route::get('master/memo-type-delete/{id}', array(
	'as' => 'delete-memotype',
	'uses' => 'MasterdataController@memoTypeDelete')
)->middleware('auth');

Route::get('master/loa', 'MasterdataController@loa')->name('loa')->middleware('auth');
Route::post('master/loa-create', 'MasterdataController@loaCreate')->middleware('auth');
Route::patch('master/loa-update/{id}', array(
	'as' => 'update-loa',
	'uses' => 'MasterdataController@loaUpdate')
)->middleware('auth');
Route::get('master/loa-delete/{id}', array(
	'as' => 'delete-loa',
	'uses' => 'MasterdataController@loaDelete')
)->middleware('auth');

Route::get('master/memo', 'MasterdataController@memo')->name('memo')->middleware('auth');
Route::get('master/memo-create', 'MasterdataController@memoCreate')->middleware('auth');
Route::get('master/memo-edit/{id}', 'MasterdataController@memoEdit')->middleware('auth');
Route::patch('master/memo-update/{id}', array(
	'as' => 'update-memo',
	'uses' => 'MasterdataController@memoUpdate')
)->middleware('auth');
Route::post('master/memo-store', array(
	'as' => 'store-memo',
	'uses' => 'MasterdataController@memoStore')
)->middleware('auth');
Route::get('master/memo-view/{id}', array(
	'as' => 'view-memo',
	'uses' => 'MasterdataController@memoView')
)->middleware('auth');
Route::get('master/memo-delete/{id}', array(
	'as' => 'delete-memo',
	'uses' => 'MasterdataController@memoDelete')
)->middleware('auth');

Route::get('setting/approval-type', 'SettingController@approvalType')->middleware('auth');
Route::post('setting/approval-type-create', 'SettingController@approvalTypeCreate')->middleware('auth');
Route::patch('setting/approval-type-update/{id}', array(
	'as' => 'update-approval-type',
	'uses' => 'SettingController@approvalTypeUpdate')
)->middleware('auth');
Route::get('setting/approval-type-delete/{id}', array(
	'as' => 'delete-approval-type',
	'uses' => 'SettingController@approvalTypeDelete')
)->middleware('auth');

Route::get('setting/memo-approver', 'SettingController@memoApprover')->middleware('auth');
Route::get('setting/memo-approver-view/{id}', 'SettingController@memoApproverView')->middleware('auth');
Route::post('setting/memo-approver-create', 'SettingController@memoApproverCreate')->middleware('auth');
Route::patch('setting/memo-approver-update/{id}', array(
	'as' => 'update-memo-approver',
	'uses' => 'SettingController@memoApproverUpdate')
)->middleware('auth');
Route::get('setting/memo-approver-delete/{id}', array(
	'as' => 'delete-memo-approver',
	'uses' => 'SettingController@memoApproverDelete')
)->middleware('auth');

Route::get('setting/groups', 'SettingController@group')->middleware('auth');
Route::post('setting/groups-create', 'SettingController@groupCreate')->middleware('auth');
Route::patch('setting/groups-update/{id}', array(
	'as' => 'update-group',
	'uses' => 'SettingController@groupUpdate')
)->middleware('auth');
Route::get('setting/groups-delete/{id}', array(
	'as' => 'delete-group',
	'uses' => 'SettingController@groupDelete')
)->middleware('auth');

Route::get('setting/users', 'SettingController@users')->middleware('auth');
Route::patch('setting/users-update/{id}', array(
	'as' => 'update-users',
	'uses' => 'SettingController@usersUpdate')
)->middleware('auth');
Route::get('setting/users-generate', 'SettingController@generateNewUsers')->middleware('auth');
Route::get('setting/users-update-username', 'SettingController@updateAllUsername')->middleware('auth');

Route::get('trans/memo', 'MemoController@index')->middleware('auth');
Route::get('trans/memo-create', 'MemoController@create')->middleware('auth');
Route::post('trans/memo-store', array(
	'as' => 'store-memo-trans',
	'uses' => 'MemoController@store')
)->middleware('auth');
Route::get('trans/memo-view/{id}', 'MemoController@show')->middleware('auth');
Route::post('trans/memo-submit-approval', 'MemoController@submitApproval');
Route::post('trans/memo-approver-submit', 'MemoController@approverSubmit');
Route::post('trans/memo-comment-submit', 'MemoController@submitComment');
Route::post('trans/memo-reply-comment', 'MemoController@replyComment');
Route::get('trans/memo-print/{id}', 'MemoController@printMemo')->middleware('auth');
Route::get('trans/memo-edit/{id}', 'MemoController@edit')->middleware('auth');
Route::patch('trans/memo-update/{id}', array(
	'as' => 'update-memo-trans',
	'uses' => 'MemoController@update')
)->middleware('auth');

Route::get('trans/memo-approval-request', 'MemoController@memoApprovalRequest')->middleware('auth');

Route::get('setting/app-setting', 'SettingController@appSetting')->middleware('auth');

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard')->middleware('auth');

Route::post('password/change', 'SettingController@changePassword')->middleware('auth');