CREATE TABLE `group_permission` (
	`perm_id` INT NOT NULL,
	`group_id` INT NOT NULL,
	PRIMARY KEY (`perm_id`, `group_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('1', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('2', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('3', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('4', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('5', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('6', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('7', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('8', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('9', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('10', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('11', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('12', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('13', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('14', '1');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('2', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('3', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('4', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('5', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('6', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('7', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('8', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('9', '2');
INSERT INTO `db_ememo`.`group_permission` (`perm_id`, `group_id`) VALUES ('10', '2');
