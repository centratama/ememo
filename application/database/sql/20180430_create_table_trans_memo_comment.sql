CREATE TABLE `trans_memo_comment` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`trans_memo_id` INT NOT NULL,
	`from` VARCHAR(8) NOT NULL,
	`to` VARCHAR(8) NOT NULL,
	`message` VARCHAR(1000) NOT NULL,
	`created_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_trans_memo_id` FOREIGN KEY (`trans_memo_id`) REFERENCES `trans_memo` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `trans_memo_comment`
	ADD COLUMN `parent_id` INT NULL DEFAULT NULL AFTER `message`;

ALTER TABLE `trans_memo_comment`
	CHANGE COLUMN `parent_id` `parent_id` INT(11) NULL DEFAULT '0' AFTER `message`;