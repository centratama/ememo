CREATE TABLE `user_group` (
	`user_id` INT NOT NULL,
	`group_id` INT NOT NULL,
	PRIMARY KEY (`user_id`, `group_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;