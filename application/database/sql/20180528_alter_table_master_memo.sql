ALTER TABLE `master_memo`
	CHANGE COLUMN `entity_id` `entity_id` CHAR(38) NULL DEFAULT NULL AFTER `memo_code`,
	CHANGE COLUMN `division_id` `division_id` CHAR(38) NULL DEFAULT NULL AFTER `entity_id`,
	CHANGE COLUMN `dept_id` `dept_id` CHAR(38) NULL DEFAULT NULL AFTER `division_id`;