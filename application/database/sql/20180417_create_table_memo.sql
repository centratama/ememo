CREATE TABLE `master_memo` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`memo_code` VARCHAR(50) NOT NULL,
	`entity_id` CHAR(38) NOT NULL,
	`division_id` CHAR(38) NOT NULL,
	`dept_id` CHAR(38) NOT NULL,
	`memo_name` VARCHAR(255) NOT NULL,
	`remarks` VARCHAR(1000) NOT NULL,
	`memotype_id` INT(11) NOT NULL,
	`memoformat_id` INT(11) NOT NULL,
	`initiated_id` INT(11) NOT NULL,
	`is_site` TINYINT(1) NULL DEFAULT NULL,
	`is_last_approval` TINYINT(1) NOT NULL,
	`is_deleted` TINYINT(1) NOT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	`deleted_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_memotype` (`memotype_id`),
	INDEX `FK_memoformat` (`memoformat_id`),
	INDEX `FK_initiated` (`initiated_id`),
	CONSTRAINT `FK_initiated` FOREIGN KEY (`initiated_id`) REFERENCES `master_initiated` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_memoformat` FOREIGN KEY (`memoformat_id`) REFERENCES `master_memoformat` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_memotype` FOREIGN KEY (`memotype_id`) REFERENCES `master_memotype` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
ENGINE=InnoDB;

ALTER TABLE `master_memo`
	ADD UNIQUE INDEX `memo_code` (`memo_code`);

ALTER TABLE `master_memo`
	CHANGE COLUMN `is_deleted` `is_deleted` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_last_approval`;

ALTER TABLE `master_memo`
	ADD COLUMN `is_zone` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_site`;

CREATE TABLE `memo_loa` (
	`memo_id` INT(11) NOT NULL,
	`loa_id` INT(11) NOT NULL,
	PRIMARY KEY (`memo_id`, `loa_id`)
)
ENGINE=InnoDB;
