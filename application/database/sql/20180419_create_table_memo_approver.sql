CREATE TABLE `memo_approver` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`memo_id` INT NOT NULL,
	`nik` INT NOT NULL,
	`approval_id` INT NOT NULL,
	`order` INT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_memo` FOREIGN KEY (`memo_id`) REFERENCES `master_memo` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_approval` FOREIGN KEY (`approval_id`) REFERENCES `master_approval` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `memo_approver`
	ALTER `nik` DROP DEFAULT;
ALTER TABLE `memo_approver`
	CHANGE COLUMN `nik` `nik` VARCHAR(8) NOT NULL AFTER `memo_id`;