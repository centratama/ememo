CREATE TABLE `master_menu` (
	`menu_id` INT NOT NULL AUTO_INCREMENT,
	`menu_name` VARCHAR(100) NOT NULL,
	`link` VARCHAR(100) NOT NULL,
	`slug` VARCHAR(255) NOT NULL,
	`parent_menu` INT NULL DEFAULT NULL,
	`sort` INT NULL DEFAULT NULL,
	`menu_status` INT NULL DEFAULT '1',
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`menu_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `db_ememo`.`master_menu` (`menu_name`, `link`, `slug`, `created_at`) VALUES ('<i class="mdi mdi-gauge"></i>Dashboard', 'dashboard', 'dashboard', '2018-04-09 22:16:22');
INSERT INTO `db_ememo`.`master_menu` (`menu_name`, `created_at`) VALUES ('<i class="mdi mdi-table-large"></i>Master Data', '2018-04-09 22:20:20');
INSERT INTO `db_ememo`.`master_menu` (`menu_name`, `link`, `slug`, `parent_menu`, `sort`, `created_at`) VALUES ('Initiated', 'master/initiated', 'master_initiated', '2', '1', '2018-04-09 22:22:34');
INSERT INTO `db_ememo`.`master_menu` (`menu_name`, `link`, `slug`, `parent_menu`, `sort`, `created_at`) VALUES ('Memo Format', 'master/memo-format', 'master_memo-format', '2', '2', '2018-04-09 22:25:57');
INSERT INTO `db_ememo`.`master_menu` (`menu_name`, `link`, `slug`, `parent_menu`, `sort`, `created_at`) VALUES ('Memo Type', 'master/memo-type', 'master_memo-type', '2', '3', '2018-04-09 22:27:37');

ALTER TABLE `master_menu`
	ALTER `slug` DROP DEFAULT;
ALTER TABLE `master_menu`
	CHANGE COLUMN `slug` `menu_perms` VARCHAR(255) NOT NULL AFTER `link`;

-- Menu Permissions
CREATE TABLE `permission_menu` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`definition` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('dashboard', 'dashboard');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master', 'master');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_initiated', 'master initiated');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_initiated_create', 'master initiated create');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_initiated_update', 'master initiated update');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_initiated_delete', 'master initiated delete');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-format', 'master memo-format');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-format_create', 'master memo-format create');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-format_update', 'master memo-format update');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-format_delete', 'master memo-format delete');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-type', 'master memo-type');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-type_create', 'master memo-type create');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-type_update', 'master memo-type update');
INSERT INTO `db_ememo`.`permission_menu` (`name`, `definition`) VALUES ('master_memo-type_delete', 'master memo-type delete');

ALTER TABLE `master_menu`
	ADD COLUMN `menu_idtag` VARCHAR(5) NOT NULL DEFAULT '' AFTER `parent_menu`;

UPDATE `db_ememo`.`master_menu` SET `menu_idtag`='md' WHERE  `menu_id`=2;