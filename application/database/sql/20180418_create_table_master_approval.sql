CREATE TABLE `master_approval` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`approval_name` VARCHAR(100) NOT NULL,
	`is_deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	`deleted_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;