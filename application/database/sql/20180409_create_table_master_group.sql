CREATE TABLE `master_group` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`desc` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `db_ememo`.`master_group` (`name`, `desc`) VALUES ('Admin', 'Super Admin');
INSERT INTO `db_ememo`.`master_group` (`name`, `desc`) VALUES ('PMO', 'Project Management Officer');
