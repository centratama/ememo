CREATE TABLE `user_permission` (
	`perm_id` INT(11) NOT NULL,
	`user_id` INT(11) NOT NULL,
	PRIMARY KEY (`perm_id`, `user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
