CREATE TABLE `trans_memo_approval` (
	`id` INT NOT NULL,
	`trans_memo_id` INT NOT NULL,
	`approval_id` INT NOT NULL,
	`approver` VARCHAR(8) NOT NULL,
	`dateInitiated` DATETIME NOT NULL,
	`comment` TEXT NOT NULL,
	`status` TINYINT(2) NULL DEFAULT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_trans_memo` FOREIGN KEY (`trans_memo_id`) REFERENCES `trans_memo` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_approval_id` FOREIGN KEY (`approval_id`) REFERENCES `master_approval` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `trans_memo_approval`
	CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `trans_memo_approval`
	ALTER `dateInitiated` DROP DEFAULT,
	ALTER `created_at` DROP DEFAULT,
	ALTER `updated_at` DROP DEFAULT;
ALTER TABLE `trans_memo_approval`
	CHANGE COLUMN `dateInitiated` `dateInitiated` DATETIME NULL AFTER `approver`,
	CHANGE COLUMN `comment` `comment` TEXT NULL AFTER `dateInitiated`,
	CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL AFTER `status`,
	CHANGE COLUMN `updated_at` `updated_at` DATETIME NULL AFTER `created_at`;
ALTER TABLE `trans_memo_approval`
	CHANGE COLUMN `status` `status` TINYINT(2) NULL DEFAULT '0' AFTER `comment`;