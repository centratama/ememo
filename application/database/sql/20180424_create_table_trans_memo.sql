CREATE TABLE `trans_memo` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`memo_id` INT NULL,
	`char_id` INT(2) NOT NULL,
	`background` VARCHAR(2000) NOT NULL,
	`background_attachment` VARCHAR(1000) NOT NULL,
	`consideration` VARCHAR(2000) NOT NULL,
	`consideration_attachment` VARCHAR(1000) NOT NULL,
	`points_to_be_app` VARCHAR(2000) NOT NULL,
	`points_to_be_app_attachment` VARCHAR(1000) NOT NULL,
	`is_active` TINYINT(1) NOT NULL DEFAULT '1',
	`created_by` VARCHAR(8) NOT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_memo_id` FOREIGN KEY (`memo_id`) REFERENCES `master_memo` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `trans_memo`
	ALTER `background` DROP DEFAULT,
	ALTER `background_attachment` DROP DEFAULT,
	ALTER `consideration` DROP DEFAULT,
	ALTER `consideration_attachment` DROP DEFAULT,
	ALTER `points_to_be_app` DROP DEFAULT,
	ALTER `points_to_be_app_attachment` DROP DEFAULT;
ALTER TABLE `trans_memo`
	CHANGE COLUMN `background` `background` TEXT NULL AFTER `char_id`,
	CHANGE COLUMN `background_attachment` `background_attachment` TEXT NULL AFTER `background`,
	CHANGE COLUMN `consideration` `consideration` TEXT NULL AFTER `background_attachment`,
	CHANGE COLUMN `consideration_attachment` `consideration_attachment` TEXT NULL AFTER `consideration`,
	CHANGE COLUMN `points_to_be_app` `points_to_be_app` TEXT NULL AFTER `consideration_attachment`,
	CHANGE COLUMN `points_to_be_app_attachment` `points_to_be_app_attachment` TEXT NULL AFTER `points_to_be_app`;
ALTER TABLE `trans_memo`
	ADD CONSTRAINT `FK_char_id` FOREIGN KEY (`char_id`) REFERENCES `master_char` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `trans_memo`
	ADD COLUMN `trans_memo_code` VARCHAR(20) NOT NULL AFTER `id`,
	ADD UNIQUE INDEX `trans_memo_code` (`trans_memo_code`);
ALTER TABLE `trans_memo`
	ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_active`;
ALTER TABLE `trans_memo`
	ADD COLUMN `comment` VARCHAR(1000) NULL DEFAULT NULL AFTER `status`,
	ADD COLUMN `initiated_date` DATETIME NULL DEFAULT NULL AFTER `comment`;