ALTER TABLE `users`
	ADD COLUMN `username` VARCHAR(8) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `id`,
	ADD UNIQUE INDEX `username` (`username`);