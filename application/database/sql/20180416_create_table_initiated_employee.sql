CREATE TABLE `initiated_employee` (
	`nik` VARCHAR(8) NOT NULL,
	`initiated_id` INT(11) NOT NULL,
	PRIMARY KEY (`nik`, `initiated_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
