CREATE TABLE `master_parameter` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`param_grp` VARCHAR(50) NOT NULL,
	`param_value` VARCHAR(50) NOT NULL,
	`param_desc` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB;
