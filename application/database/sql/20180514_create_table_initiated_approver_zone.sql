CREATE TABLE `initiated_approver_zone` (
	`nik` VARCHAR(8) NULL,
	`approval_id` INT NOT NULL COMMENT '0 = Initiated',
	`order` INT NULL DEFAULT NULL,
	`zone` INT NOT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `initiated_approver_zone`
	ALTER `zone` DROP DEFAULT;
ALTER TABLE `initiated_approver_zone`
	CHANGE COLUMN `zone` `zone` VARCHAR(50) NOT NULL AFTER `order`;